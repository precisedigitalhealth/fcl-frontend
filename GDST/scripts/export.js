var GDSTExporter = {
    JSON: {
        Export: function (ctes) {

            var epcisDocument = {
                "id": GDST.Util.GenerateUUID(),
                "isA": "EPCISDocument",
                "schemaVersion": 2.0,
                "creationDate": (new Date()).toISOString(),
                "format":"application/ld+json",
                "epcisBody": {

                }
            }

            var masterData = {
                vocabularyList: []
            };

            // serialize the product definitions
            var productDefs = {
                "isA" : "Vocabulary",
                "type" : "urn:epcglobal:epcis:vtype:EPCClass",
                "vocabularyElementList" : []
            };
            for (var i = 0; i < GDSTMasterData.ProductDefinitions.length; i++) {
                var pDef = GDSTMasterData.ProductDefinitions[i];
                var pDefJSON = {
                    "isA" : "VocabularyElement",
                    "id" : pDef.GTIN,
                    "cbvmda:descriptionShort" : pDef.Name,
                    "cbvmda:speciesForFisheryStatisticsPurposesName" : pDef.Species,
                    "cbvmda:additionalTradeItemIdentification" : pDef.ProductForm?.Code
                };
                productDefs.vocabularyElementList.push(pDefJSON);
            }
            masterData.vocabularyList.push(productDefs);

            // serialize the locations
            var locations = {
                "isA" : "VocabularyElement",
                "type" : "urn:epcglobal:epcis:vtype:Location",
                "vocabularyElementList" : []
            };
            for (var i = 0; i < GDSTMasterData.Locations.length; i++){
                var location = GDSTMasterData.Locations[i];
                var locationJSON = {
                    "isA" : "VocabularyElement",
                    "id" : location.GLN,
                    "cbvmda:name" : location.Name,
                    "cbvmda:streetAddressOne" : location.Address1,
                    "cbvmda:streetAddressTwo" : location.Address2,
                    "cbvmda:city" : location.City,
                    "cbvmda:state" : location.State,
                    "cbvmda:countryCode" : location.Country,
                    "cbvmda:vesselID" : location.VesselID,
                    "cbvmda:imoNumber" : location.IMONumber,
                    "cbvmda:vesselFlagState" : location.VesselFlag,
                    "cbvmda:unloadingPort" : location.UnloadingPort
                };
                locations.vocabularyElementList.push(locationJSON);
            }
            masterData.vocabularyList.push(locations);

            // serialize the trading partners
            var tradingparties = {
                "isA" : "VocabularyElement",
                "type" : "urn:epcglobal:epcis:vtype:TradingParty",
                "vocabularyElementList" : []
            };
            for (var i = 0; i < GDSTMasterData.TradingParties.length; i++){
                var tp = GDSTMasterData.TradingParties[i];
                var tpJSON = {
                    "isA" : "VocabularyElement",
                    "id" : tp.PGLN,
                    "cbvmda:name" : tp.Name,
                }
                tradingparties.vocabularyElementList.push(tpJSON);
            }
            masterData.vocabularyList.push(tradingparties);
            epcisDocument.epcisBody.vocabularyList = masterData.vocabularyList;

            // serialize the critical tracking events
            var epcisCTEs = [];
            for (var i = 0; i < ctes.length; i++) {
                var cte = ctes[i];

                // make sure that the event time is a Date
                if (!(cte.EventTime instanceof Date)) {
                    cte.EventTime = new Date(Date.parse(cte.EventTime));
                }
                cte.EventTimeOffSet = `${cte.EventTime.getTimezoneOffset() >= 0 ? "+" : "-"}${GDST.Util.Pad(cte.EventTime.getTimezoneOffset() / 60, 2)}:00`;

                // make sure the cte has a GUID
                if (GDST.Util.IsStringNullOrEmpty(cte.Guid)) cte.Guid = GDST.Util.GenerateUUID();

                var epcisCTE = {};
                epcisCTE.id = cte.Guid;
                epcisCTE.action = cte.EventType.Action;
                epcisCTE.isA = cte.EventType.EPCISClass;
                epcisCTE.bizStep = cte.EventType.BusinessStep;
                epcisCTE.disposition = cte.EventType.Disposition;
                epcisCTE.bizLocation = { id: cte.Location?.GLN };
                epcisCTE.eventTime = cte.EventTime.toISOString();
                epcisCTE.eventTimeZoneOffset = cte.EventTimeOffSet;

                // information provider and product owner
                epcisCTE["gdst:productOwner"] = cte.ProductOwner?.PGLN;
                epcisCTE["cbvmda:informationProvider"] = cte.InformationProvider?.PGLN;

                // if there is a buyer selected...
                epcisCTE.destinationList = [];
                if (cte.Buyer) {
                    epcisCTE.destinationList.push({
                        type: "urn:epcglobal:cbv:sdt:owning_party",
                        destination: cte.Buyer?.PGLN
                    });
                }

                // if there is a seller selected...
                epcisCTE.sourceList = [];
                if (cte.Seller) {
                    epcisCTE.sourceList.push({
                        type: "urn:epcglobal:cbv:sdt:owning_party",
                        source: cte.Seller?.PGLN
                    });
                }

                // certificates..
                epcisCTE["cbvmda:certificationList"] = [];
                for (var prop in cte.Certificates) {
                    var cert = cte.Certificates[prop];
                    var certType = cte.EventType.Certificates[prop];
                    var gs1Certificate = {
                        "gdst:certificationType" :  certType.Type,
                        "cbvmda:certificationStandard" : cert.Standard,
                        "cbvmda:certificationAgency" : cert.Agency,
                        "cbvmda:certificationValue" : cert.Value,
                        "cbvmda:certificationIdentification" : cert.Identification
                    };
                    epcisCTE["cbvmda:certificationList"].push(gs1Certificate);
                }
                
                // if this is an object event
                if (cte.EventType.EPCISClass.toLowerCase() === "object") {

                    // add the parent ID
                    epcisCTE.epcList = [];
                    if (cte.ParentID) {
                        epcisCTE.epcList.push(cte.ParentID);
                    }

                    // add the quantity list objects...
                    epcisCTE.quantityList = [];
                    if(cte.References) {
                        for (var j = 0; j < cte.References.length; j++) {
                            var epc = GDSTExporter.Utility.BuildEPC(cte.References[j]?.Product.GTIN, cte.References[j]?.LotNumber);
                            var item = {
                                epcClass: epc,
                                quantity: cte.References[j].NetWeight?.Value,
                                uom: cte.References[j].NetWeight?.UoM?.UNCode
                            };
                            epcisCTE.quantityList.push(item);
                        }
                    }
                }
                else if (cte.EventType.EPCISClass.toLowerCase() === "transformation") {

                    // add the inputs...
                    // add the quantity list objects...
                    epcisCTE.inputQuantityList = [];
                    if(cte.Inputs) {
                        for (var j = 0; j < cte.Inputs.length; j++) {
                            var epc = GDSTExporter.Utility.BuildEPC(cte.Inputs[j]?.Product.GTIN, cte.Inputs[j]?.LotNumber);
                            var item = {
                                epcClass: epc,
                                quantity: cte.Inputs[j].NetWeight?.Value,
                                uom: cte.Inputs[j].NetWeight?.UoM?.UNCode
                            };
                            epcisCTE.inputQuantityList.push(item);
                        }
                    }

                    // add the outputs...
                    // add the quantity list objects...
                    epcisCTE.outputQuantityList = [];
                    if(cte.Outputs) {
                        for (var j = 0; j < cte.Outputs.length; j++) {
                            var epc = GDSTExporter.Utility.BuildEPC(cte.Outputs[j]?.Product.GTIN, cte.Outputs[j]?.LotNumber);
                            var item = {
                                epcClass: epc,
                                quantity: cte.Outputs[j].NetWeight?.Value,
                                uom: cte.Outputs[j].NetWeight?.UoM?.UNCode
                            };
                            epcisCTE.outputQuantityList.push(item);
                        }
                    }
                }
                else if(cte.EventType.EPCISClass.toLowerCase() === "aggregate") {

                    // add the parent ID
                    epcisCTE.parentID = cte.ParentID;

                    // add the inputs...
                    // add the quantity list objects...
                    epcisCTE.childQuantityList = [];
                    if(cte.Inputs) {
                        for (var j = 0; j < cte.Inputs.length; j++) {
                            var epc = GDSTExporter.Utility.BuildEPC(cte.Inputs[j]?.Product.GTIN, cte.Inputs[j]?.LotNumber);
                            var item = {
                                epcClass: epc,
                                quantity: cte.Inputs[j].NetWeight?.Value,
                                uom: cte.Inputs[j].NetWeight?.UoM?.UNCode
                            };
                            epcisCTE.childQuantityList.push(item);
                        }
                    }
                }

                // add the KDEs
                for (var prop in cte.EventType.KDEs) {
                    var value = cte[prop];
                    var type = cte.EventType.KDEs[prop].Type;
                    if (value) {
                        if (type === 'coordinates') {
                            value = `geo:${cte[prop].Latitude},${cte[prop].Longitude}`;
                        }
                        else if (type === 'date') {
                            if(!(value instanceof Date)) {
                                value = new Date(Date.parse(value));    
                            }
                            value = value.toISOString();
                        }
                        this._setValue(epcisCTE, cte.EventType.KDEs[prop].JPath, value);
                    }
                }

                // fix things into arrays if needed
                if (epcisCTE["cbvmda:vesselCatchInformationList"]) {
                    epcisCTE["cbvmda:vesselCatchInformationList"] = [epcisCTE["cbvmda:vesselCatchInformationList"]];
                }

                epcisCTEs.push(epcisCTE);
            }

            epcisDocument.epcisBody.eventList = epcisCTEs;

            return epcisDocument;
        },

        _setValue: function(object, jPath, value) {
            var pathParts = jPath.split('.');
            if (pathParts.length > 1) {
                var path = pathParts[0];
                if (!object[path]) {
                    object[path] = {};
                }
                var newJPath = "";
                for (var i = 1; i < pathParts.length; i++) {
                    if (i > 1) { newJPath += "."; }
                    newJPath += pathParts[i];
                }
                this._setValue(object[path], newJPath, value);
            }
            else {
                object[jPath] = value;
            }
        }
    },

    Utility: {
        BuildEPC: function(gtin, lotNumber) {
            if(!gtin) gtin = "";
            if(!lotNumber) lotNumber = "";
            var epc = gtin;
            if(epc.indexOf(':product:class:') >= 0) {
                epc = epc.replace(':product:class:', ':product:lot:class:');
            }
            else if (epc.indexOf(':idpat:sgtin:')) {
                epc = epc.replace(':idpat:sgtin:', ':id:lgtin:');
            }
            else {
                GDST.Util.Alert('Error', `The GTIN '${gtin}' is not valid.`);
            }
            return epc + '.' + lotNumber;
        }
    }
};