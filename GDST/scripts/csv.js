class GDSTCSVConverter {

    constructor(shapes, callback) {
        var self = this;

        // sync the master data
        GDSTMasterData.SyncMasterData();

        // sort the shapes
        //self._shapes = shapes.sort((a, b) => Date.parse(a.dataItem.EventTime) - Date.parse(b.dataItem.EventTime));
        self._shapes = shapes
        // get the csv from the templates
        this._getCSVTemplates(function () {

            // add the master data
            self._addMasterData();

            // go through each shape sorted by the event time
            for (var i = 0; i < self._shapes.items.length; i++) {
                var cte = self._shapes.items[i].options.dataItem;

                // make sure the cte has a GUID
                if (GDST.Util.IsStringNullOrEmpty(cte.Guid)) cte.Guid = GDST.Util.GenerateUUID();

                // make sure that the event time is a Date
                if (!(cte.EventTime instanceof Date)) {
                    cte.EventTime = new Date(Date.parse(cte.EventTime));
                }
                cte.EventTimeOffSet = `${cte.EventTime.getTimezoneOffset() >= 0 ? "+" : "-"}${GDST.Util.Pad(cte.EventTime.getTimezoneOffset() / 60, 2)}:00`;

                // if the cte is of epcis class 'object'...
                if (cte.EventType.EPCISClass.toLowerCase().trim() === 'object') {
                    self._addObjectEvent(cte);
                }

                // if the cte is of epcis class 'transformation'...
                if (cte.EventType.EPCISClass.toLowerCase().trim() === 'transformation') {
                    self._addTransformationEvent(cte);
                }

                // if the cte is of epcis class 'aggregate'...
                if (cte.EventType.EPCISClass.toLowerCase().trim() === 'aggregate') {
                    self._addAggregateEvent(cte);
                }
            }

            // DEBUG PURPOSES
            // self._downloadCSV(self.objectCSV, 'object.csv');

            // bundle the CSV and convert it into XML using the Trawler module
            window.trawler.BuildXMLFromCSV({
                "businessHeaderCsv": self.headerCSV,
                "epcClassCsv": self.productDefCSV,
                "locationCsv": self.locationCSV,
                "objectEventCsv": self.objectCSV,
                "transformationEventCsv": self.transformationCSV,
                "aggregationEventCsv": self.aggregateCSV
            }).then(function (result) {
                self.xml = result;
                if (callback) {

                    // add in the trading parties to the XML
                    var xTradingParties = `<Vocabulary type="urn:epcglobal:epcis:vtype:TradingParty">
                                                <VocabularyElementList>`
                    for (var i = 0; i < GDSTMasterData.TradingParties.length; i++) {
                        var tp = GDSTMasterData.TradingParties[i];
                        var xTP = ` <VocabularyElement id="${tp.PGLN}">
                                        <attribute id="urn:epcglobal:cbv:mda#name">${tp.Name}</attribute>
                                    </VocabularyElement>`;
                        xTradingParties += xTP;
                    }
                    xTradingParties += `    </VocabularyElementList>
                                        </Vocabulary>`;
                    var jXML = $($.parseXML(self.xml));
                    var test = jXML.xpath('//EPCISMasterData/VocabularyList').html();   
                    jXML.xpath('//EPCISMasterData/VocabularyList').append(xTradingParties);
                    var xml2string = function (xmlData) { 

                        var xmlString;
                        //IE
                        if (window.ActiveXObject){
                            xmlString = xmlData.xml;
                        }
                        // code for Mozilla, Firefox, Opera, etc.
                        else{
                            xmlString = (new XMLSerializer()).serializeToString(xmlData);
                        }
                        return xmlString;
                    }   
                    self.xml = xml2string(jXML[0]);

                    // for debug purposes we are going to convert the generated XML back into a 
                    // list of CTEs, then we are going to compare the deserialized value to the 
                    // CTEs we used to serialize into the XML. For the most part this can be commented
                    // out in the production environment.
                    // var data = GDST.WebApplication.ImportTool._importXML(self.xml);
                    // for (var i = 0; i < self._shapes.length; i++) {
                    //     var cte = self._shapes[i].dataItem;
                    //     var deserializedCTE = data.CTEs.find(c => c.Guid === cte.Guid);

                    //     if (deserializedCTE) {
                    //         GDST.Util.CompareObjects(cte, deserializedCTE, ['UUID', 'Color', 'GuidD', 'uid']);
                    //     }
                    //     else {
                    //         console.log('FAILED TO DESERRIALIZE CTE!');
                    //     }
                    // }

                    callback();
                }
            });
        })
    }

    /** Triggers the generated XML to be downloaded. */
    DownloadXML() {
        //this._downloadCSV(this.objectCSV, 'objects.csv');
        this._downloadXML(this.xml);
    }

    /** Add the master data to the CSV. */
    _addMasterData() {
        var self = this;

        // create the header row
        var headerRow = new GDSTCSVRow(self.headerMapping);
        headerRow.AddValue('senderId', 'test');
        headerRow.AddValue('senderName', 'test');
        headerRow.AddValue('senderEmail', 'test@test.com');
        headerRow.AddValue('receiverId', 'test');
        headerRow.AddValue('receiverName', 'test');
        headerRow.AddValue('receiverEmail', 'test@test.com');
        self.headerCSV += headerRow.ToString();

        // add the locations
        for (var i = 0; i < GDSTMasterData.Locations.length; i++) {
            var location = GDSTMasterData.Locations[i];
            var locationRow = new GDSTCSVRow(self.locationMapping);
            locationRow.AddValue("id", location.GLN ?? "");
            locationRow.AddValue("name", location.Name);
            locationRow.AddValue("streetAddressOne", location.Address1);
            locationRow.AddValue("streetAddressTwo", location.Address2);
            locationRow.AddValue("city", location.City);
            locationRow.AddValue("state", location.State);
            locationRow.AddValue("countryCode", location.Country);
            locationRow.AddValue("vesselID", location.VesselID);
            locationRow.AddValue("imoNumber", location.IMONumber);
            locationRow.AddValue("vesselFlagState", location.VesselFlag);
            locationRow.AddValue("unloadingPort", location.UnloadingPort);
            var locationRowString = locationRow.ToString();
            self.locationCSV += locationRowString;
        }

        // add the products
        for (var i = 0; i < GDSTMasterData.ProductDefinitions.length; i++) {
            var pi = GDSTMasterData.ProductDefinitions[i];
            var productDefRow = new GDSTCSVRow(self.productDefMapping);
            productDefRow.AddValue("id", pi.GTIN ?? "");
            productDefRow.AddValue("descriptionShort", pi.Name);
            productDefRow.AddValue("speciesForFisheryStatisticsPurposesName", pi.Species);
            productDefRow.AddValue("additionalTradeItemIdentification", pi.ProductForm?.Code);
            var productDefRowString = productDefRow.ToString();
            self.productDefCSV += productDefRowString;
        }
    }

    /** Add the object type events to the CSV. */
    _addObjectEvent(cte) {
        var self = this;
        var objectRow = new GDSTCSVRow(this.objectMapping);

        objectRow.AddValue("action", cte.EventType.Action);
        objectRow.AddValue("eventId", cte.Guid);
        objectRow.AddValue("bizStep", cte.EventType.BusinessStep);
        objectRow.AddValue("informationProvider", cte.InformationProvider?.PGLN ?? "");
        objectRow.AddValue("productOwner", cte.ProductOwner?.PGLN ?? "");

        if (cte.Seller) {
            objectRow.AddValue("extension.sourceList.source.type", "owning_party");
            objectRow.AddValue("extension.sourceList.source.value", cte.Seller?.PGLN ?? "");
        }
        else {
            objectRow.AddValue("extension.sourceList.source.type", " ");
            objectRow.AddValue("extension.sourceList.source.value", " ");
        }

        if (cte.Buyer) {
            objectRow.AddValue("extension.destinationList.destination.type", "owning_party");
            objectRow.AddValue("extension.destinationList.destination.value", cte.Buyer?.PGLN ?? "");
        }
        else {
            objectRow.AddValue("extension.destinationList.destination.type", " ");
            objectRow.AddValue("extension.destinationList.destination.value", " ");
        }

        objectRow.AddValue("disposition", cte.EventType.Disposition);
        objectRow.AddValue("bizLocation.id", cte.Location?.GLN ?? "");
        objectRow.AddValue("eventTime", cte.EventTime);
        objectRow.AddValue("eventTimeZoneOffset", cte.EventTimeOffSet);

        // add any location mappings
        var location = cte.Location;
        if (location && cte.EventType.LocationMappings) {
            for (var i = 0; i < cte.EventType.LocationMappings.length; i++) {
                var mapping = cte.EventType.LocationMappings[i];
                var locationValue = location[mapping.LocationPath];
                if (locationValue) {
                    objectRow.AddValue(mapping.CSVPath, locationValue);
                }
            }
        }

        // add in the KDEs
        for (var kdeProperty in cte.EventType.KDEs) {
            var csvName = cte.EventType.KDEs[kdeProperty].CSVName;
            var type = cte.EventType.KDEs[kdeProperty].Type;
            var value = cte[kdeProperty];
            if (value) {
                if (type === 'coordinates') {
                    value = `"geo:${cte[kdeProperty].Latitude},${cte[kdeProperty].Longitude}"`;
                }
                objectRow.AddValue(csvName, value);
            }
        }

        // add the certificates
        if (cte.Certificates) {
            for (var certProperty in cte.EventType.Certificates) {
                var certificate = cte.Certificates[certProperty];
                if (certificate) {
                    var certificateType = cte.EventType.Certificates[certProperty].Type;
                    if (certificateType === 'urn:gdst:cert:humanPolicy') {
                        objectRow.AddValue("humanWelfarePolicy", certificate.Standard);
                    }
                    objectRow.AddValue("extension.ilmd.certificationList.certification.certificationType", certificateType ?? "");
                    objectRow.AddValue("extension.ilmd.certificationList.certification.certificationStandard", certificate.Standard ?? "");
                    objectRow.AddValue("extension.ilmd.certificationList.certification.certificationValue", certificate.Value ?? "");
                    objectRow.AddValue("extension.ilmd.certificationList.certification.certificationAgency", certificate.Agency ?? "");
                    objectRow.AddValue("extension.ilmd.certificationList.certification.certificationIdentification", certificate.Identification ?? "");
                }
            }
        }

        // add the product instances
        for (var j = 0; j < cte.References.length; j++) {
            var pi = cte.References[j];

            // if this is a serial instance EPC...
            var classEPC = GDSTExporter.Utility.BuildEPC(pi.Product?.GTIN, pi.LotNumber);
            objectRow.AddValue("extension.quantityList.quantityElement.epcClass", classEPC);
            objectRow.AddValue("extension.quantityList.quantityElement.quantity", pi.NetWeight.Value);
            objectRow.AddValue("extension.quantityList.quantityElement.uom", pi.NetWeight.UoM.UNCode);
        }

        // add the SCC if we have one
        if (!GDST.Util.IsStringNullOrEmpty(cte.ParentID)) {
            objectRow.AddValue("epcList.epc", cte.ParentID);
        }

        // add the row to the CSV we are building
        this.objectCSV += objectRow.ToString();
    }

    /** Add the transformation type events to the CSV. */
    _addTransformationEvent(cte) {
        var self = this;
        var transformationRow = new GDSTCSVRow(this.transformationMapping);

        transformationRow.AddValue("action", cte.EventType.Action);
        transformationRow.AddValue("bizStep", cte.EventType.BusinessStep);
        transformationRow.AddValue("eventId", cte.Guid);
        transformationRow.AddValue("informationProvider", cte.InformationProvider?.PGLN ?? "");
        transformationRow.AddValue("productOwner", cte.ProductOwner?.PGLN ?? "");
        transformationRow.AddValue("disposition", cte.EventType.Disposition);
        transformationRow.AddValue("bizLocation.id", cte.Location?.GLN ?? "");
        transformationRow.AddValue("eventTime", cte.EventTime);
        transformationRow.AddValue("eventTimeZoneOffset", cte.EventTimeOffSet);

        // add in the KDEs
        for (var kdeProperty in cte.EventType.KDEs) {
            var csvName = cte.EventType.KDEs[kdeProperty].CSVName;
            var value = cte[kdeProperty];
            transformationRow.AddValue(csvName, value);
        }

        // add the certificates
        if (cte.EventType.Certificates) {
            for (var certProperty in cte.EventType.Certificates) {
                if (cte.Certificates) {
                    var certificate = cte.Certificates[certProperty];
                    if (certificate) {
                        var certificateType = cte.EventType.Certificates[certProperty].Type;
                        if (certificateType === 'urn:gdst:cert:humanPolicy') {
                            transformationRow.AddValue("humanWelfarePolicy", certificate.Standard);
                        }
                        transformationRow.AddValue("ilmd.certificationList.certification.certificationType", certificateType);
                        transformationRow.AddValue("ilmd.certificationList.certification.certificationStandard", certificate.Standard);
                        transformationRow.AddValue("ilmd.certificationList.certification.certificationValue", certificate.Value);
                        transformationRow.AddValue("ilmd.certificationList.certification.certificationAgency", certificate.Agency);
                        transformationRow.AddValue("ilmd.certificationList.certification.certificationIdentification", certificate.Identification);
                    }
                }
            }
        }

        // add the input product instances
        for (var j = 0; j < cte.Inputs.length; j++) {
            var pi = cte.Inputs[j];

            var classEPC = GDSTExporter.Utility.BuildEPC(pi.Product?.GTIN, pi.LotNumber);
            transformationRow.AddValue("inputQuantityList.quantityElement.epcClass", classEPC);
            transformationRow.AddValue("inputQuantityList.quantityElement.quantity", pi.NetWeight.Value);
            transformationRow.AddValue("inputQuantityList.quantityElement.uom", pi.NetWeight.UoM.UNCode);
        }

        // add the output product instances
        for (var j = 0; j < cte.Outputs.length; j++) {
            var pi = cte.Outputs[j];

            // if this is a serial instance EPC...
            var classEPC = GDSTExporter.Utility.BuildEPC(pi.Product?.GTIN, pi.LotNumber);
            transformationRow.AddValue("outputQuantityList.quantityElement.epcClass", classEPC);
            transformationRow.AddValue("outputQuantityList.quantityElement.quantity", pi.NetWeight.Value);
            transformationRow.AddValue("outputQuantityList.quantityElement.uom", pi.NetWeight.UoM.UNCode);
        }

        // add the row to the CSV we are building
        this.transformationCSV += transformationRow.ToString();
    }

    /** Add the aggregate type events to the CSV. */
    _addAggregateEvent(cte) {
        var self = this;
        var aggregateRow = new GDSTCSVRow(this.aggregateMapping);

        aggregateRow.AddValue("action", cte.EventType.Action);
        aggregateRow.AddValue("bizStep", cte.EventType.BusinessStep);
        aggregateRow.AddValue("eventId", cte.Guid);
        aggregateRow.AddValue("informationProvider", cte.InformationProvider?.PGLN ?? "");
        aggregateRow.AddValue("productOwner", cte.ProductOwner?.PGLN ?? "");
        aggregateRow.AddValue("disposition", cte.EventType.Disposition);
        aggregateRow.AddValue("bizLocation.id", cte.Location?.GLN ?? "");
        aggregateRow.AddValue("eventTime", cte.EventTime);
        aggregateRow.AddValue("eventTimeZoneOffset", cte.EventTimeOffSet);

        // add in the KDEs
        for (var kdeProperty in cte.EventType.KDEs) {
            var csvName = cte.EventType.KDEs[kdeProperty].CSVName;
            var value = cte[kdeProperty];
            aggregateRow.AddValue(csvName, value);
        }

        // add the certificates
        if (cte.EventType.Certificates && cte.Certificates) {
            for (var certProperty in cte.EventType.Certificates) {
                var certificate = cte.Certificates[certProperty];
                if (certificate) {
                    var certificateType = cte.EventType.Certificates[certProperty].Type;
                    if (certificateType === 'urn:gdst:cert:humanPolicy') {
                        aggregateRow.AddValue("humanWelfarePolicy", certificate.Standard);
                    }
                    aggregateRow.AddValue("extension.certificationList.certification.certificationType", certificateType);
                    aggregateRow.AddValue("extension.certificationList.certification.certificationStandard", certificate.Standard);
                    aggregateRow.AddValue("extension.certificationList.certification.certificationValue", certificate.Value);
                    aggregateRow.AddValue("extension.certificationList.certification.certificationAgency", certificate.Agency);
                    aggregateRow.AddValue("extension.certificationList.certification.certificationIdentification", certificate.Identification);
                }
            }
        }


        // add the parent ID
        aggregateRow.AddValue("parentID", cte.ParentID);

        // add the child product instances
        for (var j = 0; j < cte.Inputs.length; j++) {
            var pi = cte.Inputs[j];

            // if this is a serial instance EPC...
            var classEPC = GDSTExporter.Utility.BuildEPC(pi.Product?.GTIN, pi.LotNumber);
            aggregateRow.AddValue("extension.childQuantityList.quantityElement.epcClass", classEPC);
            aggregateRow.AddValue("extension.childQuantityList.quantityElement.quantity", pi.NetWeight.Value);
            aggregateRow.AddValue("extension.childQuantityList.quantityElement.uom", pi.NetWeight.UoM.UNCode);
        }

        // add the row to the CSV we are building
        this.aggregateCSV += aggregateRow.ToString();
    }

    /** Get the CSV files. */
    _getCSVTemplates(callback) {
        var self = this;

        // download the template csv files
        self.productDefCSV = "";
        self.locationCSV = "";
        self.objectCSV = "";
        self.transformationCSV = "";
        self.aggregateCSV = "";
        self.headerCSV = "";
        var promises = [];
        promises.push(GDST.Util.DownloadCSV("./csv/locations.csv", function (csv) {
            self.locationCSV = csv + "\n";
        }));
        promises.push(GDST.Util.DownloadCSV("./csv/productDefs.csv", function (csv) {
            self.productDefCSV = csv + "\n";
        }));
        promises.push(GDST.Util.DownloadCSV("./csv/objects.csv", function (csv) {
            self.objectCSV = csv + "\n";
        }));
        promises.push(GDST.Util.DownloadCSV("./csv/transformations.csv", function (csv) {
            self.transformationCSV = csv + "\n";
        }));
        promises.push(GDST.Util.DownloadCSV("./csv/aggregations.csv", function (csv) {
            self.aggregateCSV = csv + "\n";
        }));
        promises.push(GDST.Util.DownloadCSV("./csv/header.csv", function (csv) {
            self.headerCSV = csv + "\n";
        }));

        // wait for all the CSV files to downloade
        Promise.all(promises).then(async function () {

            // create mappings for the CSV files
            self.headerMapping = self._mapCSV(self.headerCSV);
            self.locationMapping = self._mapCSV(self.locationCSV);
            self.productDefMapping = self._mapCSV(self.productDefCSV);
            self.objectMapping = self._mapCSV(self.objectCSV);
            self.transformationMapping = self._mapCSV(self.transformationCSV);
            self.aggregateMapping = self._mapCSV(self.aggregateCSV);
            self.headerMapping = self._mapCSV(self.headerCSV);

            // callback if provided
            if (callback) {
                callback();
            }
        });
    }

    /** Download the XML generated from the CSV. */
    _downloadXML(xml) {
        xml = vkbeautify.xml(xml);
        var contentType = 'text/xml;charset=utf-8;';
        var xmlFile = new Blob([xml], { type: contentType });
        var a = document.createElement('a');
        a.download = 'epcis.xml';
        a.href = window.URL.createObjectURL(xmlFile);
        a.textContent = 'Download XML';
        a.dataset.downloadurl = [contentType, a.download, a.href].join(':');
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    /** Download the CSV. */
    _downloadCSV(csv, name) {
        var contentType = 'text/csv;charset=utf-8;';
        var csvFile = new Blob([csv], { type: contentType });
        console.log(csvFile);
        var a = document.createElement('a');
        a.download = name + '.csv';
        a.href = window.URL.createObjectURL(csvFile);
        a.textContent = 'Download CSV';
        a.dataset.downloadurl = [contentType, a.download, a.href].join(':');
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    /** Create a mapping from the CSV file. */
    _mapCSV(csv) {
        var mapping = new Map();
        var reverseMapping = new Map();
        var rows = csv.split('\n');
        for (var i = (rows.length - 1); i >= 0; i--) {
            var pieces = rows[i].split(',');
            for (var j = 0; j < pieces.length; j++) {
                var columnName = pieces[j];
                if (!GDST.Util.IsStringNullOrEmpty(reverseMapping.get(j))) {
                    for (var m = j; m >= 0; m--) {
                        columnName = pieces[m];
                        if (!GDST.Util.IsStringNullOrEmpty(columnName)) {
                            break;
                        }
                    }
                    columnName = columnName + "." + reverseMapping.get(j);
                }
                reverseMapping.set(j, columnName);
            }
        }
        reverseMapping.forEach(function (value, key, map) {
            mapping.set(value.trim(), key);
        });
        return mapping;
    }
}

class GDSTCSVRow {

    constructor(mapping) {
        this._mapping = mapping;
        this._row = new Map();
    }

    /** Adds a value to the CSV Row. */
    AddValue(columnName, value) {
        if (!this._row.get(columnName)) {
            this._row.set(columnName, []);
        }
        this._row.get(columnName).push(value ?? "");
    }

    /** Converts the GDST Row into a string. */
    ToString() {
        var self = this;

        // go through the mapping one value at a time in order of the index...
        var r = 0;
        var rowString = "";
        while (r < 100) {
            var rString = "";
            var foundValue = false;
            self._mapping.forEach(function (value, key, map) {
                var list = self._row.get(key) ?? [];
                if (list.length > r) {
                    foundValue = true;
                    var value = list[r];
                    if (value instanceof Date) {
                        value = value.toISOString()
                    }
                    else if (key.toLowerCase() === "eventtime") {
                        var date = new Date(Date.parse(value));
                        value = date.toISOString()
                    }
                    rString += value;
                }
                rString += ",";
            });

            rString = rString.substring(rString, rString.length - 1);

            if (foundValue === false) {
                break;
            }
            else {
                rowString += rString + '\n';
            }

            r++;
        }
        return rowString;
    }
}