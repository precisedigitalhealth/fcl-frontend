class GDSTMeasurement {

    static _uoms;
    static get UOMs() {
        if(!GDSTMeasurement._uoms)
        {
            var uoms = [
                new GDSTUnitOfMeasurement("Kilograms", "kg", "Weight", "KGM"),
                new GDSTUnitOfMeasurement("Pounds", "lb", "Weight", "LBR")
            ];
    
            // add a map...
            uoms.Map = new Map();
            for(var i = 0; i < uoms.length; i++)
            {
                uoms.Map.set(uoms[i].Name, uoms[i]);
                uoms.Map.set(uoms[i].Abbreviation, uoms[i]);
                uoms.Map.set(uoms[i].UNCode, uoms[i]);
            }

            GDSTMeasurement._uoms = uoms;
        }
        return GDSTMeasurement._uoms;
    }

    /** Given a UNCode, Abbreviation, or Name, this will return the GDSTUnitOfMeasurment object. IF a 
     *  GDSTUnitOfMeasurement object is passed, then it just returns the argument passed into it.
     */
    static LookUpUOM(uom) {
        if(uom instanceof GDSTUnitOfMeasurement)
        {
            return uom;
        }
        else {
            var lookedUpUOM = GDSTMeasurement.UOMs.Map.get(uom);
            return lookedUpUOM;
        }
    }

    constructor(value, uom) {
        this.Value = value;
        this.UoM = GDSTMeasurement.LookUpUOM(uom);
    }
}

class GDSTUnitOfMeasurement {
    constructor(name, abbreviation, dimension, unCode) {
        this.Name = name;
        this.Abbreviation = abbreviation;
        this.Dimension = dimension;
        this.UNCode = unCode;
    }
}