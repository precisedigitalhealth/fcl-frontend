let GDSTMasterData = {

    GenerateNewTradingPartyColors: function(e) {
        if(e) {
            $(e.currentTarget).find('.fa').removeClass('fa-redo').addClass('fa-spin fa-spinner');
            $(e.currentTarget).find('span').text('Generating...');
        }
        
        // blank all the colors out first
        for (var i = 0; i < GDSTMasterData.TradingParties.length; i++){
            GDSTMasterData.TradingParties[i].Color = null;
        }
        // go through each trading party and assign a new color
        for (var i = 0; i < GDSTMasterData.TradingParties.length; i++){
            GDSTMasterData.TradingParties[i].Color = GDST.Util.GenerateRandomColor();
        }

        var j = 0;
        var generate = function() {
            GDSTMasterData.TradingParties[j].Color = GDST.Util.GenerateRandomColor();
            j++;

            if(j < GDSTMasterData.TradingParties.length) {
                setTimeout(generate, 100);
            }
            else {
                // go through each cte and reset the trading party
                for (var i = 0; i < GDST.WebApplication.Diagram.KendoDiagram.shapes.length; i++) {
                    var shape = GDST.WebApplication.Diagram.KendoDiagram.shapes[i];
                    shape.dataItem.ProductOwner = GDSTMasterData.TradingParties.find(t => t.PGLN == shape.dataItem.ProductOwner?.PGLN);
                    shape.redrawVisual();
                }
                GDST.WebApplication.Diagram.RebindShapes();
                if(e) {
                    $(e.currentTarget).find('.fa').addClass('fa-redo').removeClass('fa-spin fa-spinner');
                    $(e.currentTarget).find('span').text('Generate New Trading Party Colors');
                }
            }
        }
        setTimeout(generate, 250);
    },

    /** This goes through and syncs the master data with the ctes. */
    SyncMasterData: function () {
        var shapes = GDST.WebApplication.Diagram.KendoDiagram.markers;
        for (var i = 0; i < shapes.items.length; i++) {
            var cte = shapes.items[i].options.dataItem;

            // sync the location
            cte.Location = GDSTMasterData.Locations.find(l => l.GLN === cte.Location?.GLN);

            // sync the trading parties
            cte.Buyer = GDSTMasterData.TradingParties.find(t => t.PGLN === cte.Buyer?.PGLN);
            cte.Seller = GDSTMasterData.TradingParties.find(t => t.PGLN === cte.Seller?.PGLN);
            cte.ProductOwner = GDSTMasterData.TradingParties.find(t => t.PGLN === cte.ProductOwner?.PGLN);
            cte.InformationProvider = GDSTMasterData.TradingParties.find(t => t.PGLN === cte.InformationProvider?.PGLN);

            // sync the product definitions
            var ___syncPDefs = function(list) {
                if (list) {
                    for (var j = 0; j < list.length; j++) {
                        list[j].Product = GDSTMasterData.ProductDefinitions.find(p => p.GTIN === list[j].Product?.GTIN);
                    }
                }
            }
            ___syncPDefs(cte.References);
            ___syncPDefs(cte.Inputs);
            ___syncPDefs(cte.Outputs);

            // redraw the shape
            //shapes[i].redrawVisual();
        }

        GDST.WebApplication.Diagram.RebindShapes();
        //GDSTFeedback.CanPerformTraceback(GDST.WebApplication.Diagram.KendoDiagram.shapes, GDST.WebApplication.Diagram.KendoDiagram.connections);
    },

    TradingParties: new kendo.data.ObservableArray([]),
    ProductDefinitions: new kendo.data.ObservableArray([]),
    Locations: new kendo.data.ObservableArray([]),

    /** An instance of the master data editor window. */
    Window: class {

        constructor(settings) {
            var self = this;
            self.Element = $(settings.element);

            // initialize the kendo window
            this._kendoWindow = $(settings.element).kendoWindow({
                title: 'Master Data',
                width: '75%',
                visible: false,
                animation: false,
                activate: function(e) {
                    self.Resize();
                },
                resize: function(e) {
                    self.Resize();         
                },
                close: function(e) {
                    // sync the master data
                    GDSTMasterData.SyncMasterData();
                }
            }).data('kendoWindow');

            // initialize and bind the view model
            this._vm = new kendo.observable({

                // data sources
                TradingParties: GDSTMasterData.TradingParties,
                ProductDefinitions: GDSTMasterData.ProductDefinitions,
                Locations: GDSTMasterData.Locations,

                // static data
                ProductForms: [
                    { Code: 'BMS', Name: 'Below Minimum Conservation Reference Size' },
                    { Code: 'CBF', Name: 'Cod butterfly Escalado' },
                    { Code: 'CLA', Name: 'Claws' },
                    { Code: 'CUT_FOR_SALE', Name: 'Cut For Sale' },
                    { Code: 'DWT', Name: 'ICCAT code' },
                    { Code: 'FIL', Name: 'Filleted' },
                    { Code: 'FIS', Name: 'Filleted and Skinned Fillets' },
                    { Code: 'FSB', Name: 'Filleted with Skin and Bones' },
                    { Code: 'FSP', Name: 'Filleted Skinned with Pinbone On' },
                    { Code: 'GHT', Name: 'Gutted Headed and Tailed' },
                    { Code: 'GUG', Name: 'Gutted and Gilled' },
                    { Code: 'GUH', Name: 'Gutted and Headed' },
                    { Code: 'GUL', Name: 'Gutted Liver In' },
                    { Code: 'GUS', Name: 'Gutted Headed and Skinned' },
                    { Code: 'GUT', Name: 'Gutted' },
                    { Code: 'HEA', Name: 'Headed' },
                    { Code: 'JAP', Name: 'Japanese Cut' },
                    { Code: 'JAT', Name: 'Tailed Japanese Cut' },
                    { Code: 'LAP', Name: 'Lappen' },
                    { Code: 'LVR', Name: 'Liver' },
                    { Code: 'LVR_C', Name: 'Liver-C' },
                    { Code: 'OTH', Name: 'Other' },
                    { Code: 'ROE', Name: 'Roe (s)' },
                    { Code: 'ROE_C', Name: 'Roe (s) - C' },
                    { Code: 'SAD', Name: 'Headed with Skin On, Spine On, Tail On and Salted Dry' },
                    { Code: 'SAL', Name: 'Headed with Skin On, Spine On, Tail On and Salted' },
                    { Code: 'SGH', Name: 'Salted, Gutted and Headed' },
                    { Code: 'SGT', Name: 'Salted Gutted' },
                    { Code: 'SKI', Name: 'Skinned' },
                    { Code: 'SUR', Name: 'Surimi' },
                    { Code: 'TAL', Name: 'Tail' },
                    { Code: 'TLD', Name: 'Tailed' },
                    { Code: 'TNG', Name: 'Tongue' },
                    { Code: 'TNG_C', Name: 'Tongue - Collective Presentation' },
                    { Code: 'TO_BE_CUT_FOR_SALE', Name: 'To Be Cut For Sale' },
                    { Code: 'TO_BE_PORTIONED', Name: 'To Be Portioned' },
                    { Code: 'TUB', Name: 'Tube Only' },
                    { Code: 'WHL', Name: 'Whole' },
                    { Code: 'WNG', Name: 'Wings' }
                ],

                // trading partners
                SelectedTradingParty: null,
                BuildPGLN: function(e) {
                    var self = this;
                    GDST.WebApplication.GLNEditor.CreateGLN(function(pgln) {
                        self.SelectedTradingParty.set('PGLN', pgln);
                        self.OnPGLNChanged();
                    }, true);
                },
                AddTradingParty: function(e) {

                    // generate a color for the trading party
                    var color = GDST.Util.GenerateRandomColor();

                    this.TradingParties.push({ UUID: GDST.Util.GenerateUUID(), Name: "New Trading Party", PGLN: "", Color: color });
                    this.set('SelectedTradingParty', this.TradingParties[this.TradingParties.length - 1]);
                    this.RememberSelectedTradingParty();

                    // select and highlight the name text
                    $("#tp-name").focus().select();
                },
                DeleteTradingParty: function(e) {
                    
                    // find all CTEs using this trading party and set it to null
                    var diagram = GDST.WebApplication.Diagram;
                    for(var i = 0; i < diagram.KendoDiagram.shapes.length; i++) {
                        var shape = diagram.KendoDiagram.shapes[i];
                        if(shape.dataItem.ProductOwner?.UUID == this.SelectedTradingParty.UUID){
                            shape.dataItem.ProductOwner = null;
                        }
                        if(shape.dataItem.InformationProvider?.UUID == this.SelectedTradingParty.UUID){
                            shape.dataItem.InformationProvider = null;
                        }
                        if(shape.dataItem.Buyer?.UUID == this.SelectedTradingParty.UUID){
                            shape.dataItem.Buyer = null;
                        }
                        if(shape.dataItem.Seller?.UUID == this.SelectedTradingParty.UUID){
                            shape.dataItem.Seller = null;
                        }
                        shape.redrawVisual();
                    }
                    GDST.WebApplication.Diagram.RebindShapes();

                    // delete the trading party
                    this.TradingParties.remove(this.SelectedTradingParty);
                    this.set('SelectedTradingParty', null);
                },
                TradingPartyChanged: function(e) {
                    var data = e.sender.dataItem(e.sender.select());
                    this.set('SelectedTradingParty', data);
                    if (this.SelectedTradingParty) {
                        this.SelectedTradingParty.set('OldPGLN', data.PGLN);
                    }
                },
                OnTradingPartyNameChanged: function(e) {
                    var listview = self.Element.find('.trading-party-list').data('kendoListView');
                    listview.refresh();
                    this.RememberSelectedTradingParty();
                },
                OnPGLNChanged: function(e) {
                    var oldPGLN = this.SelectedTradingParty.OldPGLN;
                    var pgln = this.SelectedTradingParty.PGLN;

                    // go through and update the PGLN on any CTEs.
                    for (var i = 0; i < GDST.WebApplication.Diagram.KendoDiagram.shapes.length; i++) {
                        var shape = GDST.WebApplication.Diagram.KendoDiagram.shapes[i];
                        var cte = shape.dataItem;
                        if (cte.ProductOwner?.PGLN === oldPGLN) {
                            cte.ProductOwner.PGLN = pgln;
                        }
                        if (cte.InformationProvider?.PGLN === oldPGLN) {
                            cte.InformationProvider.PGLN = pgln;
                        }
                    }
                    this.SelectedTradingParty.set('OldPGLN', data.PGLN);
                },
                RememberSelectedTradingParty: function() {
                    var listview = self.Element.find('.trading-party-list').data('kendoListView');

                    // make sure we reset the selected listview item
                    var items = listview.items();
                    if(items.length > 0) {
                        var item = items[0];
                        if(this.SelectedTradingParty) {
                            for(var i = 0; i < listview.items().length; i++)
                            {
                                var data = listview.dataItem(listview.items()[i]);
                                if(data.uid == this.SelectedTradingParty.uid)
                                {
                                    item = items[i];;
                                    break;
                                }
                            }
                        }
                        listview.select(item);
                    }
                    else {
                        listview.select({});
                    }
                },

                // product definitions
                SelectedProductDefinition: null,
                BuildGTIN: function(e) {
                    var self = this;
                    GDST.WebApplication.GTINEditor.CreateGTIN(function(gtin) {
                        self.SelectedProductDefinition.set('GTIN', gtin);
                        self.OnGTINChanged();
                    });
                },
                AddProductDefinition: function(e) {
                    this.ProductDefinitions.push({ UUID: GDST.Util.GenerateUUID(), Name: "New Product Definition", GTIN: "" });
                    this.set('SelectedProductDefinition', this.ProductDefinitions[this.ProductDefinitions.length - 1]);
                    this.RememberSelectedProductDefinition();

                    // select and highlight the name text
                    $("#product-name").focus().select();
                },
                DeleteProductDefinition: function(e) {

                    // find all CTEs using this trading party and set it to null
                    var diagram = GDST.WebApplication.Diagram;
                    for(var i = 0; i < diagram.KendoDiagram.shapes.length; i++) {
                        var shape = diagram.KendoDiagram.shapes[i];

                        for(var j = 0; j < shape.dataItem.References.length; j++)
                        {
                            var product = shape.dataItem.References[j];
                            if(product.Product?.UUID == this.SelectedProductDefinition.UUID)
                            {
                                product.Product = null;
                            }
                        }

                        for(var j = 0; j < shape.dataItem.Inputs.length; j++)
                        {
                            var product = shape.dataItem.Inputs[j];
                            if(product.Product?.UUID == this.SelectedProductDefinition.UUID)
                            {
                                product.Product = null;
                            }
                        }

                        for(var j = 0; j < shape.dataItem.Outputs.length; j++)
                        {
                            var product = shape.dataItem.Outputs[j];
                            if(product.Product?.UUID == this.SelectedProductDefinition.UUID)
                            {
                                product.Product = null;
                            }
                        }

                        shape.redrawVisual();
                    }
                    GDST.WebApplication.Diagram.RebindShapes();
                    GDSTFeedback.CanPerformTraceback(GDST.WebApplication.Diagram.KendoDiagram.shapes, GDST.WebApplication.Diagram.KendoDiagram.connections);

                    // delete the product definition
                    this.ProductDefinitions.remove(this.SelectedProductDefinition);
                    this.set('SelectedProductDefinition', null);
                },
                ProductDefinitionChanged: function(e) {
                    var data = e.sender.dataItem(e.sender.select());
                    this.set('SelectedProductDefinition', data);
                    if (this.SelectedProductDefinition) {
                        this.SelectedProductDefinition.set('OldGTIN', data.GTIN);
                    }
                },
                OnProductDefinitionNameChanged: function(e) {
                    var listview = self.Element.find('.product-def-list').data('kendoListView');
                    listview.refresh();
                    this.RememberSelectedProductDefinition();
                },
                OnGTINChanged: function (e) {
                    var oldGTIN = this.SelectedProductDefinition.OldGTIN;
                    var gtin = this.SelectedProductDefinition.GTIN;

                    // go through and update the GTIN on any CTEs.
                    for (var i = 0; i < GDST.WebApplication.Diagram.KendoDiagram.shapes.length; i++) {
                        var shape = GDST.WebApplication.Diagram.KendoDiagram.shapes[i];
                        var cte = shape.dataItem;
                        if (cte.References) {
                            for (var j = 0; j < cte.References.length; j++) {
                                if (cte.References[i].Product?.GTIN === oldGTIN) {
                                    cte.References[i].Product.GTIN = gtin;
                                }
                            }
                        }
                        if (cte.Inputs) {
                            for (var j = 0; j < cte.Inputs.length; j++) {
                                if (cte.Inputs[i].Product?.GTIN === oldGTIN) {
                                    cte.Inputs[i].Product.GTIN = gtin;
                                }
                            }
                        }
                        if (cte.Outputs) {
                            for (var j = 0; j < cte.Outputs.length; j++) {
                                if (cte.Outputs[i].Product?.GTIN === oldGTIN) {
                                    cte.Outputs[i].Product.GTIN = gtin;
                                }
                            }
                        }
                    }
                    this.SelectedTradingParty.set('OldGTIN', data.PGLN);
                },
                RememberSelectedProductDefinition: function() {
                    var listview = self.Element.find('.product-def-list').data('kendoListView');

                    // make sure we reset the selected listview item
                    var items = listview.items();
                    if(items.length > 0) {
                        var item = items[0];
                        if(this.SelectedProductDefinition) {
                            for(var i = 0; i < listview.items().length; i++)
                            {
                                var data = listview.dataItem(listview.items()[i]);
                                if(data.uid == this.SelectedProductDefinition.uid)
                                {
                                    item = items[i];;
                                    break;
                                }
                            }
                        }
                        listview.select(item);
                    }
                    else {
                        listview.select({});
                    }
                },


                // locations
                SelectedLocation: null,
                BuildGLN: function(e) {
                    var self = this;
                    GDST.WebApplication.GLNEditor.CreateGLN(function(gln) {
                        self.SelectedLocation.set('GLN', gln);
                        self.OnGLNChanged();
                    });
                },
                AddLocation: function(e) {
                    this.Locations.push({ UUID: GDST.Util.GenerateUUID(), Name: "New Location", GLN: "" });
                    this.set('SelectedLocation', this.Locations[this.Locations.length - 1]);
                    this.RememberSelectedLocation();

                    // select and highlight the name text
                    $("#location-name").focus().select();
                },
                DeleteLocation: function(e) {

                    // find all CTEs using this location and set it to null
                    var diagram = GDST.WebApplication.Diagram;
                    for(var i = 0; i < diagram.KendoDiagram.shapes.length; i++) {
                        var shape = diagram.KendoDiagram.shapes[i];
                        if(shape.dataItem.Location?.UUID == this.SelectedLocation.UUID){
                            shape.dataItem.Location = null;
                        }
                        shape.redrawVisual();
                    }
                    GDST.WebApplication.Diagram.RebindShapes();

                    // delete the location
                    this.Locations.remove(this.SelectedLocation);
                    this.set('SelectedLocation', null);
                },
                LocationChanged: function(e) {
                    var data = e.sender.dataItem(e.sender.select());
                    this.set('SelectedLocation', data);
                },
                OnLocationNameChanged: function(e) {
                    var listview = self.Element.find('.location-list').data('kendoListView');
                    listview.refresh();
                    this.RememberSelectedLocation();
                },
                RememberSelectedLocation: function() {
                    var listview = self.Element.find('.location-list').data('kendoListView');

                    // make sure we reset the selected listview item
                    var items = listview.items();
                    if(items.length > 0) {
                        var item = items[0];
                        if(this.SelectedLocation) {
                            for(var i = 0; i < listview.items().length; i++)
                            {
                                var data = listview.dataItem(listview.items()[i]);
                                if(data.uid == this.SelectedLocation.uid)
                                {
                                    item = items[i];;
                                    break;
                                }
                            }
                        }
                        listview.select(item);
                    }
                    else {
                        listview.select({});
                    }
                }
            });
            kendo.bind($(settings.element), this._vm);

            // resize everything
            // this.Resize();
        }

        /** Resize the content in the window. */
        Resize() {
            var self = this;
            self.Element.find('.tab-panel').height($(window).height() * 0.5);
            self._kendoWindow.center();
        }

        /** Opens the window for editing the master data. */
        Open() {
            this._vm.RememberSelectedTradingParty();
            this._vm.RememberSelectedLocation();
            this._vm.RememberSelectedProductDefinition();
            this._kendoWindow.center().open();
        }

        /** Closes the window for editing the master data. */
        Close() {
            this._kendoWindow.close();
        }
    },

    /** Returns the Hex code for the color assigned to the trading party. */
    GetTradingPartyColor: function(tp) {

    },
}