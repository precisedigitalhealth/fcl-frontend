class GDSTHelp {

    constructor(settings) {
        var self = this;
        self.Element = settings.element;
        
        // initialize the kendo window
        self._window = self.Element.kendoWindow({
            title: 'Help',
            height: '90%',
            width: '75%',
            visible: false,
            resize: function (e) {
                self.Resize();
            },
            open: function (e) {
                e.sender.setOptions({
                    height: $(window).height() * 0.75,
                    width: $(window).width() * 0.75
                });
                e.sender.center();
            },
            activate: function (e) {
                self.Resize();
            },
            close: function(e) {
                $("#help-video").html(`<iframe width="1366" height="600" src="https://www.youtube.com/embed/kgT37r7XWFk" 
                frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                allowfullscreen></iframe>`);
            }
        }).data('kendoWindow');

        // embed the PDF document
        GDST.Util.EmbedFromURL('https://ift-gftc.github.io/IT-Conversion-Mapping-Tool/HelpDocumentation.pdf', $("#help-document"));

        // bind to the view model
        self.ViewModel = new kendo.observable({

        });
        kendo.bind(self.Element, self.ViewModel);
    }

    Resize() {
        $("#help-document").height(this.Element.height() - 40);
    }

    Open() {
        this._window.center().open();
    }
}

jQuery.fn.outerHTML = function(s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};