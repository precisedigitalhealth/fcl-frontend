// Sconst { isObject } = require("./trawler-bundled");

let GDST = {

    EventTypes: null,

    /** Feedback methods for analyzing CTEs and providing feedback. */
    Feedback: {
        IsCTEComplete: function (cte) {


        }
    },

    /** Web Application stuff. */
    WebApplication: {

        /** Initializes the web application. */
        Initialize: function () {

            GDST.WebApplication.Resize();

            // download the config json
            //$.ajax({
            //    url: './config.json',
            //    cache: false,
            //    success: function (config) {

            // set the event types

            GDST.EventTypes = config.EventTypes;
            GDST.EventTypes.FindByBizStep = function (bizStep) {
                for (var prop in GDST.EventTypes) {
                    var eType = GDST.EventTypes[prop];
                    if (eType.BusinessStep === bizStep) {
                        return eType;
                    }
                }
            };

            // initialize the gtin editor
            GDST.WebApplication.GTINEditor = new GTINEditor();

            // initialize the gtin editor
            GDST.WebApplication.GLNEditor = new GLNEditor();

            // initialize the import tool
            GDST.WebApplication.ImportTool = new GDSTXmlImporter($("#import-window"));
            GDST.WebApplication.ImportJSONTool = new GDSTJsonImporter($("#import-json-window"));

            // initalize the diagram 
            GDST.WebApplication.Diagram = new GDSTDiagram({
                element: $("#diagram"),
                eventTypes: config.EventTypes,
                onDoubleClickItem: function (shape) {
                    GDST.WebApplication.CTEEditor.EditCTE(shape);
                }
            });

            // initialize the cte editor
            GDST.WebApplication.CTEEditor = new GDSTEventEditor({
                element: $("#cte-form-container"),
                eventTypes: config.EventTypes
            });

            // initailize the master data editor
            GDST.WebApplication.MasterDataEditor = new GDSTMasterData.Window({
                element: $("#master-data-window")
            });

            // initialize the help window
            GDST.WebApplication.HelpWindow = new GDSTHelp({
                element: $("#help-window")
            });

            // bind to the help-button
            $(".help-button").on('click', function () {
                GDST.WebApplication.HelpWindow.Open();
            });

            // bind to the debug button
            $(".debug-button").on('click', function () {
                var debugJSON = {
                    browser: {
                        appName: window.navigator.appName,
                        appVersion: window.navigator.appVersion,
                        product: window.navigator.product,
                        userAgent: window.navigator.userAgent,
                        appCodeName: window.navigator.appCodeName,
                        platform: window.navigator.platform
                    },
                    data: JSON.parse(window.localStorage.getItem('diagram'))
                };
                var debugString = JSON.stringify(debugJSON, null, 2);
                GDST.Util.DownloadStringAsTextFile('debug.json', debugString);
            });

            // hide the loader
            $(".loader").hide();

            // bind to the unlock button
            $(".unlock-button").on('click', function () {
                var password = $('.unlock-textbox').val();
                if (password === 'ift') {
                    $('.password-screen').fadeOut();
                    $("#disclaimer").kendoWindow({ title: "Disclaimer" }).data('kendoWindow').center().open();
                }
            });

            // disable password screen for debug purposes
            $('.password-screen').fadeOut();
            $("#disclaimer").kendoWindow({ title: "Disclaimer" }).data('kendoWindow').center().open();
        },
                //error: function (error) {
                //    alert('There was an error loading the config.json. Please make sure it is in correct JSON format and that the file exists.');
                //    console.log(error);
                //}
            //});
        //},

        /** Resizes the web application. */
        Resize: function (e) {
            var windowHeight = window.innerHeight;
            var navigationHeight = 50;
            var contentHeight = windowHeight - navigationHeight;
            $("#navbar").height(navigationHeight);
            $("#content").height(contentHeight);

            // resize the diagram
            GDST.WebApplication.Diagram?.KendoDiagram.resize();
        },
    },

    /** Helper methods, templates, editors, and additional code to support the kendo stuff. */
    Kendo: {

        Editors: {

            MeasurementEditor: function (dimension) {
                return function (container, options) {
                    var uoms = GDSTMeasurement.UOMs.filter(u => u.Dimension.toLowerCase() === dimension.toLowerCase());

                    $('<input class="measurement-value" style="width: 60%; margin: 0px;" required name="' + options.field + '.Value"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            min: 0,
                            decimals: 4
                        });

                    $('<input class="measurement-uom" style="width: 40%; margin: 0px;" required name="' + options.field + '.UoM"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: true,
                            dataSource: uoms,
                            dataTextField: "Abbreviation",
                            dataValueField: "UNCode",
                        });
                }
            }
        },

        Templates: {

            MeasurementTemplate: function (field) {
                return function (data) {
                    var measurement = data[field];
                    if (measurement) {
                        return `${measurement.Value} ${measurement.UoM.Abbreviation}`;
                    }
                    else {
                        return "";
                    }
                }
            }
        },

        Diagram: {

            ShapeVisual: function () {
                var s = this;
                s.dataItem.EventType = GDST.EventTypes[s.dataItem.Type];

                // Render template and bind it to the current data item
                var dataItem = s.dataItem;
                var renderElement = $("<div style='display:inline-block' />").appendTo("body");
                var errorIcon = "";
                if (GDSTFeedback.IsCTEComplete(s.dataItem) === false) {
                    errorIcon = `<i class="fa fa-exclamation-triangle"
                                    style="position: absolute; top: 0.5em; right: 0.8em;"></i>`;
                }
                var colors = "";
                var fontColor = "color: white;";
                if (dataItem.ProductOwner?.Color) {
                    colors = `background-color: ${dataItem.ProductOwner.Color}; color: ${GDST.Util.DetermineFontColor(dataItem.ProductOwner.Color)};`;
                    fontColor = `color: ${GDST.Util.DetermineFontColor(dataItem.ProductOwner?.Color)};`;
                }
                var html = `<div class="diagram-item-v2" style="${colors}">
                                ${errorIcon}
                                <div>
                                    <center>
                                        <label style="font-size: 1.5em; ${fontColor}">
                                            ${s.dataItem.EventType.BigTemplateName ?? s.dataItem.EventType.Name}
                                        </label>
                                    </center>
                                    <table style="width: 15.0em; height: auto; margin-top: 0.3em; ${fontColor}">
                                        <tr>
                                            <td>
                                                <b>Date</b>
                                            </td>
                                            <td style="text-align: right;" class="truncate">
                                                ${GDST.Util.JustTheDateString(s.dataItem.EventTime) ?? ""}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <b>Owner</b>
                                            </td>
                                            <td style="text-align: right;" class="truncate">
                                                ${s.dataItem.ProductOwner?.Name ?? ""}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Location</b>
                                            </td>
                                            <td style="text-align: right;" class="truncate">
                                                ${s.dataItem.Location?.Name ?? ""}
                                            </td>
                                        </tr>`;
                if (dataItem.Buyer) {
                    html += `<tr>
                                <td>
                                    <b>Buyer</b>
                                </td>
                                <td style="text-align: right;" class="truncate">
                                    ${s.dataItem.Buyer?.Name ?? ""}
                                </td>
                            </tr>`;
                }
                html += `
                                </table>
                            </div>
                        </div>`;
                renderElement.html(html); 

                // Create a new group that will hold the rendered content
                var output = new kendo.drawing.Group();
                var width = renderElement.width();
                var height = renderElement.height();
                // Create a rectangle using the renderElement dimensions to expand the group while waiting for its actual content
                var geom = new kendo.geometry.Rect([0, 0], [width, height]);
                output.append(new kendo.drawing.Rect(geom, { stroke: { width: 0 } }));

                kendo.drawing.drawDOM(renderElement)
                    .then(function (group) {
                        /* Remove helper rectangle */
                        output.clear();
                        output.append(group);

                        /* Clean-up */
                        renderElement.remove();
                    });

                var visual = new kendo.dataviz.diagram.Group({
                    x: -7.5,
                    y: -7.5
                });
                visual.drawingElement.append(output);
                return visual;
            },
        }
    },

    /** Utility and helper methods. */
    Util: {

        Pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        },

        /** Downloads the given text as a file in UTF-8 encoding. */
        DownloadStringAsTextFile(filename, text) {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
            element.setAttribute('download', filename);

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
        },

        /** Displays an alert to the user. */
        Alert: function (title, text) {
            var self = this;
            if (!self._alertWindow) {
                $("#windows").append(`<div id="alertWindow">
                                        <center class="text"></center>
                                        <center style="margin-top: 15px;">
                                            <button class="k-button okay">Okay</button>
                                        </center>
                                      </div>`);
                self._alertWindow = $("#alertWindow").kendoWindow({
                    width: 400,
                    visible: false,
                }).data('kendoWindow');
            }

            $("#alertWindow .text").text(text);
            $("#alertWindow .okay").unbind('click');
            $("#alertWindow .okay").on('click', function () {
                self._alertWindow.close();
            });
            self._alertWindow.title(title);

            self._alertWindow.center().open();
        },

        /** Returns TRUE if the string is null or empty. Otherwise returns FALSE. */
        IsStringNullOrEmpty: function (string) {
            return (!string || /^\s*$/.test(string));
        },

        /** Download a CSV file. */
        DownloadCSV: function (url, callback) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: url,
                    success: function (csv) {
                        callback(csv);
                        resolve(csv);
                    },
                    error: function (error) {
                        reject(error);
                    }
                })
            });
        },

        /** Generate a UUID */
        GenerateUUID: function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        },

        /** Generates a random color. */
        GenerateRandomColor: function () {

            // get a list of the currently used colors
            var currentColors = GDSTMasterData.TradingParties.map(t => t.Color).filter(c => (c !== null && c !== undefined));
            var generateRandomColor = function () {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            };
            var compare2Colors = function (color1, color2) {

                // convert colors to LAB
                color1 = chroma(color1).lab();
                color2 = chroma(color2).lab();

                // calculate the delta-e
                var deltaE_pre = Math.pow((color1[0] - color2[0]), 2) +
                    Math.pow((color1[1] - color2[1]), 2) +
                    Math.pow((color1[2] - color2[2]), 2);
                var deltaE = Math.sqrt(deltaE_pre);

                // return the delta-e
                return deltaE;
            }

            // generate a random color
            // convert the color to lab
            // go through 
            var bestcolor = null;
            var bestcolorDiff = 0;
            for (var i = 0; i < 1000; i++) {
                // generate a random color
                var color = generateRandomColor();
                if (bestcolor == null) {
                    bestcolor = color;
                }

                // if there are no other colors, then just take this one
                if (currentColors.length < 1) {
                    bestcolor = color;
                    break;
                }

                // determine the smallest lab difference against all the colors
                var smallestDiff = 0;
                for (var j = 0; j < currentColors.length; j++) {
                    var diff = compare2Colors(currentColors[j], color);
                    if (diff < smallestDiff || smallestDiff === 0) {
                        smallestDiff = diff;
                    }
                }

                // if this better than the last color, then set this as the best color
                if (bestcolorDiff < smallestDiff || bestcolorDiff === 0) {
                    bestcolor = color;
                    bestcolorDiff = smallestDiff;
                }

                // if the difference is within our threshhold, then accept this new color
                if (bestcolorDiff > 100) {
                    break;
                }
            }

            return bestcolor;
        },

        /** Determine the font color based on the background color. */
        DetermineFontColor: function (color) {
            color = chroma(color).rgb();

            var d = "black";

            var luminance = (0.299 * color[0] + 0.587 * color[1] + 0.114 * color[2]) / 255;

            if (luminance > 0.5)
                d = "black"; // bright colors - black font
            else
                d = "white"; // dark colors - white font

            return d;
        },

        /**
         * Returns the date in a string format that doesn't include any timezone information or time information.
         * @param {any} date
         */
        JustTheDateString: function (date) {
            return kendo.toString(kendo.parseDate(date), 'MM/dd/yyyy')
        },

        /** Embed a PDF document into the HTML and preview it using a URL for the PDF. */
        EmbedFromURL: function (url, container) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'blob';
            xhr.onload = function (e) {
                if (this.status == 200) {
                    var blob = new Blob([this.response], { type: 'application/pdf' });
                    var blobURL = window.URL.createObjectURL(blob);
                    html = `<object style="width: 100%; height: 100%;" data="${blobURL}" type="application/pdf"></object>`;
                    container.html(html);
                }
            };
            xhr.send();
        },

        CompareObjects: function (obj1, obj2, ignoreProperties, stack) {
            if (GDST.Util.IsFunction(obj1) || GDST.Util.IsFunction(obj2)) {
                return;
            }

            if (!stack) stack = 0;
            else if (stack > 100) {
                console.log('BLEW THE STACK!');
                return;
            }
            if (GDST.Util.IsArray(obj1)) {
                for (var i = 0; i < obj1.length; i++) {
                    if (obj1[i] && obj2[i]) {
                        this.CompareObjects(obj1[i], obj2[i], ignoreProperties, stack + 1);
                    }
                }
            }
            else {
                for (var prop in obj1) {
                    if (prop.startsWith('_')) {
                        continue;
                    }
                    if (ignoreProperties.find(p => p === prop)) {
                        continue;
                    }
                    if (obj1[prop]?.length === 0 && obj2[prop]?.length === 0) {
                        continue;
                    }
                    if (GDST.Util.IsFunction(obj1[prop]) || GDST.Util.IsFunction(obj2[prop])) {
                        continue;
                    }
                    if (obj1[prop] !== undefined && obj1[prop] !== null && GDST.Util.IsObject(obj1[prop])) {
                        if (!GDST.Util.IsObject(obj2[prop])) {
                            console.log(`Property Mismatch :: ${prop}`);
                            console.log(obj1[prop]);
                            console.log(obj2[prop]);
                        }
                        else {
                            this.CompareObjects(obj1[prop], obj2[prop], ignoreProperties, stack + 1);
                        }
                    }
                    else if (obj1[prop] != obj2[prop]) {
                        var msg = `Property Mismatch :: ${prop} :: ${obj1[prop]} :: ${obj2[prop]}`;
                        console.log(msg);
                    }
                }
            }
        },

        IsArray(yourVariable) {
            return Array.isArray(yourVariable);
        },

        IsObject(yourVariable) {
            return (`${yourVariable}` === '[object Object]')
        },

        IsFunction(functionToCheck) {
            return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
        }
    }
};


// ENTRY POINT
$(document).on('ready', function () {
    GDST.WebApplication.Initialize()

    // bind to the resize event on the window
    var _resizeTimeout = null;
    $(window).resize(function () {
        console.log('resizing!');
        if (_resizeTimeout) { window.clearTimeout(_resizeTimeout); }
        _resizeTimeout = window.setTimeout(function () {
            GDST.WebApplication.Resize();
        }, 100);
    });
});

var config = {
    "EventTypes" : {
        "FishingEvent" : {
            "ID" : "FishingEvent",
            "Type" : "References",
            "Title" : "Fishing",
            "Name" : "Fishing",
            "LocationName" : "Vessel",
            "LocationMappings" : [
                { "LocationPath": "Name", "CSVPath" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselName" },
                { "LocationPath": "VesselID", "CSVPath" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselID" },
                { "LocationPath": "IMONumber", "CSVPath" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.imoNumber" },
                { "LocationPath": "VesselFlag", "CSVPath" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselFlagState" }
            ],
            "BusinessStep" : "urn:gdst:bizStep:fishingEvent",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Object",
            "KDEs" : {
                "GearType" : {
                    "Type" : "dropdownlist",
                    "Options" : [
                        { "Code" : "1", "Name" : "Surrounding Nets" },
                        { "Code" : "2", "Name" : "Seine Nets" },
                        { "Code" : "3", "Name" : "Trawl Nets" },
                        { "Code" : "4", "Name" : "Dredges" },
                        { "Code" : "5", "Name" : "Lift Nets" },
                        { "Code" : "6", "Name" : "Falling Gear" },
                        { "Code" : "7", "Name" : "Gillnets and Entangling Nets" },
                        { "Code" : "8", "Name" : "Traps" },
                        { "Code" : "9", "Name" : "Hooks and Lines" },
                        { "Code" : "10", "Name" : "Grappling and Wounding" },
                        { "Code" : "11", "Name" : "Harvesting Gear" },
                        { "Code" : "12", "Name" : "Miscellaneous" }
                    ],
                    "Name" : "Gear Type",
                    "CBVAttribute" : "fishingGearTypeCode",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.fishingGearTypeCode",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/cbvmda:fishingGearTypeCode",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.cbvmda:fishingGearTypeCode"
                },
                "PublicVesselRegistryHyperlink" : {
                    "Type" : "string",
                    "Name" : "Public Vessel Registry Hyperlink",
                    "CBVAttribute" : "vesselPublicRegistry",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselPublicRegistry",
                    "XPath": "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:vesselPublicRegistry",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.cbvmda:vesselPublicRegistry"
                },
                "HasCatchCoordinates" : {
                    "Type" : "boolean",
                    "Name" : "Has Catch Coordinates",
                    "CBVAttribute" : "gpsAvailability",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.gpsAvailability",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:gpsAvailability",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.cbvmda:gpsAvailability"
                },
                "VesselSatelliteTrackingAuthority" : {
                    "Type" : "string",
                    "Name" : "Vessel Satellite Tracking Authority",
                    "CBVAttribute" : "satelliteTracking",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.satelliteTrackingAuthority",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:satelliteTrackingAuthority",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.cbvmda:satelliteTrackingAuthority"
                },
                "CatchArea" : {
                    "Type" : "string",
                    "Name" : "Catch Area",
                    "CBVAttribute" : "catchArea",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.catchArea",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/cbvmda:catchArea",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.cbvmda:catchArea"
                },
                "EconomicZone" : {
                    "Type" : "string",
                    "Name" : "Economic Zone",
                    "CBVAttribute" : "economicZone",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.economicZone",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/cbvmda:economicZone",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.cbvmda:economicZone"
                },
                "RMFOArea" : {
                    "Type" : "string",
                    "Name" : "RMFO Area",
                    "CBVAttribute" : "rmfoArea",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.rmfoArea",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:rmfoArea",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.gdst:rmfoArea"
                },
                "SubNationalPermitArea" : {
                    "Type" : "string",
                    "Name" : "Sub-National Permit Area",
                    "CBVAttribute" : "subnationalPermitArea",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.subnationalPermitArea",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:subnationalPermitArea",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.gdst:subnationalPermitArea"
                },
                "FisheryImprovementProject" : {
                    "Type" : "string",
                    "Name" : "Fishery Improvement Project",
                    "CBVAttribute" : "fisheryImprovementProject",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.fisheryImprovementProject",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:fisheryImprovementProject",
                    "JPath" : "cbvmda:vesselCatchInformationList.cbvmda:vesselCatchInformation.gdst:fisheryImprovementProject"
                },
                "VesselTripStartDate" : {
                    "Type" : "date",
                    "Name" : "Vessel Trip Start Date",
                    "CBVAttribute" : "harvestStartDate",
                    "CSVName" : "extension.ilmd.harvestStartDate",
                    "XPath" : "extension/ilmd/cbvmda:harvestStartDate",
                    "JPath" : "ilmd.cbvmda:harvestStartDate"
                },
                "VesselTripEndDate" : {
                    "Type" : "date",
                    "Name" : "Vessel Trip End Date",
                    "CBVAttribute" : "harvestEndDate",
                    "CSVName" : "extension.ilmd.harvestEndDate",
                    "XPath" : "extension/ilmd/cbvmda:harvestEndDate",
                    "JPath" : "ilmd.cbvmda:harvestEndDate"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "FishingAuthorization" : {
                    "Type" : "urn:gdst:cert:fishingAuth",
                    "Name" : "Fishing Authorization"
                },
                "HarvestCertificate" : {
                    "Type" : "urn:gdst:cert:harvestCert",
                    "Name" : "Harvest Certification"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "TransshipmentEvent" : {
            "ID" : "TransshipmentEvent",
            "Name" : "Transship",
            "LocationName" : "Vessel",
            "Title" : "Transshipment",
            "Type" : "References",
            "BusinessStep" : "urn:gdst:bizStep:transshipment",
            "Disposition" : "active",
            "Action" : "OBSERVE",
            "EPCISClass": "Object",
            "KDEs" : {
                "TransshipmentVesselName" : {
                    "IsMasterData" : true,
                    "Type" : "string",
                    "Name" : "Transshipment Vessel Name",
                    "CBVAttribute" : "vesselName",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselName",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/cbvmda:vesselName",
                    "JPath" : "cbvmda:vesselName"
                },
                "TransshipmentUniqueVesselID" : {
                    "IsMasterData" : true,
                    "Type" : "string",
                    "Name" : "Transshipment Unique Vessel ID",
                    "CBVAttribute" : "imoNumber",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselID",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/cbvmda:vesselID",
                    "JPath" : "cbvmda:vesselID"
                },
                "TransshipmentVesselRegistration" : {
                    "IsMasterData" : true,
                    "Type" : "string",
                    "Name" : "Transshipment Vessel Registration",
                    "CBVAttribute" : "vesselID",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselPublicRegistry",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/gdst:vesselPublicRegistry",
                    "JPath" : "gdst:vesselPublicRegistry"
                },
                "TransshipmentVesselFlag" : {
                    "IsMasterData" : true,
                    "Type" : "string",
                    "Name" : "Transshipment Vessel Flag",
                    "CBVAttribute" : "vesselFlagState",
                    "CSVName" : "extension.ilmd.vesselCatchInformationList.vesselCatchInformation.vesselFlagState",
                    "XPath" : "extension/ilmd/cbvmda:vesselCatchInformationList/cbvmda:vesselCatchInformation/cbvmda:vesselFlagState",
                    "JPath" : "cbvmda:vesselFlagState"
                },
                "TransshipmentStartDate" : {
                    "Type" : "date",
                    "Name" : "Transshipment Start Date",
                    "CBVAttribute" : "transshipStartDate",
                    "CSVName" : "transshipStartDate",
                    "XPath" : "cbvmda:transshipStartDate",
                    "JPath" : "cbvmda:transshipStartDate"
                },
                "TransshipmentEndDate" : {
                    "Type" : "date",
                    "Name" : "Transshipment End Date",
                    "CBVAttribute" : "transshipEndDate",
                    "CSVName" : "transshipEndDate",
                    "XPath" : "cbvmda:transshipEndDate",
                    "JPath" : "cbvmda:transshipEndDate"
                },
                "UnloadingPort" : {
                    "Type" : "string",
                    "Name" : "Unloading Port",
                    "CBVAttribute" : "unloadingPort",
                    "CSVName" : "unloadingPort",
                    "XPath" : "cbvmda:unloadingPort",
                    "JPath" : "cbvmda:unloadingPort"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "TransshipmentAuthorization" : {
                    "Type" : "urn:gdst:cert:transshipmentAuth",
                    "Name" : "Transshipment Authorization"
                },
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "LandingEvent" : {
            "ID" : "LandingEvent",
            "Name" : "Landing",
            "LocationName" : "Port",
            "Title" : "Landing",
            "Type" : "References",
            "BusinessStep" : "urn:gdst:bizStep:landing",
            "Disposition" : "active",
            "Action" : "OBSERVE",
            "EPCISClass": "Object",
            "KDEs" : {
                "LandingStartDate" : {
                    "Type" : "date",
                    "Name" : "Landing Start Date",
                    "CBVAttribute" : "landingStartDate",
                    "CSVName" : "landingStartDate",
                    "XPath" : "cbvmda:landingStartDate",
                    "JPath" : "cbvmda:landingStartDate"
                },
                "LandingEndDate" : {
                    "Type" : "date",
                    "Name" : "Landing End Date",
                    "CBVAttribute" : "landingEndDate",
                    "CSVName" : "landingEndDate",
                    "XPath" : "cbvmda:landingEndDate",
                    "JPath" : "cbvmda:landingEndDate"
                },
                "UnloadingPort" : {
                    "Type" : "string",
                    "Name" : "Unloading Port",
                    "CBVAttribute" : "unloadingPort",
                    "CSVName" : "unloadingPort",
                    "XPath" : "cbvmda:unloadingPort",
                    "JPath" : "cbvmda:unloadingPort"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "LandingAuthorization" : {
                    "Type" : "urn:gdst:cert:landingAuth",
                    "Name" : "Landing Authorization"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "CommingleEvent" : {
            "ID" : "CommingleEvent",
            "Name" : "Commingle",
            "LocationName" : "Location",
            "Title" : "Commingle",
            "Type" : "InputsAndOutputs",
            "BusinessStep" : "urn:gdst:bizStep:commingling",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Transformation",
            "KDEs" : {
                "ProductionDate" : {
                    "Type" : "date",
                    "Name" : "Production Date",
                    "CBVAttribute" : "productionDate",
                    "CSVName" : "ilmd.productionDate",
                    "XPath" : "ilmd/cbvmda:productionDate",
                    "JPath" : "ilmd.cbvmda:productionDate"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
            }
            },
            "Certificates" : {
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "ProcessorLicense" : {
                    "Type" : "urn:gdst:cert:processorLicense",
                    "Name" : "Processor License"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "FarmHarvestEvent" : {
            "ID" : "FarmHarvestEvent",
            "Name" : "Farm<br/>Harvest",
            "LocationName" : "Farm",
            "Title" : "Farm Harvest",
            "Type" : "InputsAndOutputs",
            "BusinessStep" : "urn:gdst:bizStep:farmHarvest",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Transformation",
            "KDEs" : {
                "HarvestStartDate" : {
                    "Type" : "date",
                    "Name" : "Harvest Start Date",
                    "CBVAttribute" : "harvestStartDate",
                    "CSVName" : "ilmd.harvestStartDate",
                    "XPath" : "ilmd/cbvmda:harvestStartDate",
                    "JPath" : "ilmd.cbvmda:harvestStartDate"
                },
                "HarvestEndDate" : {
                    "Type" : "date",
                    "Name" : "Harvest End Date",
                    "CBVAttribute" : "harvestEndDate",
                    "CSVName" : "ilmd.harvestEndDate",
                    "XPath" : "ilmd/cbvmda:harvestEndDate",
                    "JPath" : "ilmd.cbvmda:harvestEndDate"
                },
                "FarmingMethod" : {
                    "Type" : "string",
                    "Name" : "Farming Method",
                    "CBVAttribute" : "aquacultureMethod",
                    "CSVName" : "ilmd.aquacultureMethod",
                    "XPath" : "ilmd/cbvmda:aquacultureMethod",
                    "JPath" : "ilmd.cbvmda:aquacultureMethod"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCertificate" : {
                    "Type" : "urn:gdst:cert:harvestCert",
                    "Name" : "Harvest Certification"
                },
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "HatcheryEvent" : {
            "ID" : "HatcheryEvent",
            "Name" : "Hatchery",
            "LocationName" : "Hatchery",
            "Title" : "Hatchery",
            "Type" : "References",
            "BusinessStep" : "urn:gdst:bizStep:hatching",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Object",
            "KDEs" : {
                "HarvestStartDate" : {
                    "Type" : "date",
                    "Name" : "Harvest Start Date",
                    "CBVAttribute" : "harvestStartDate",
                    "CSVName" : "extension.ilmd.harvestStartDate",
                    "XPath" : "extension/ilmd/cbvmda:harvestStartDate",
                    "JPath" : "ilmd.cbvmda:harvestStartDate"
                },
                "HarvestEndDate" : {
                    "Type" : "date",
                    "Name" : "Harvest End Date",
                    "CBVAttribute" : "harvestEndDate",
                    "CSVName" : "extension.ilmd.harvestEndDate",
                    "XPath" : "extension/ilmd/cbvmda:harvestEndDate",
                    "JPath" : "ilmd.cbvmda:harvestEndDate"
                },
                "BroodstockSource" : {
                    "Type" : "string",
                    "Name" : "Source of Broodstock",
                    "CBVAttribute" : "broodstockSource",
                    "CSVName" : "extension.ilmd.broodstockSource",
                    "XPath" : "extension/ilmd/gdst:broodstockSource",
                    "JPath" : "ilmd.gdst:broodstockSource"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCertificate" : {
                    "Type" : "urn:gdst:cert:harvestCert",
                    "Name" : "Harvest Certification"
                },
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "ProcessingEvent" : {
            "ID" : "ProcessingEvent",
            "Name" : "Processing",
            "LocationName" : "Processing Plant",
            "Title" : "Processing",
            "Type" : "InputsAndOutputs",
            "BusinessStep" : "urn:epcglobal:cbv:bizstep:commissioning",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Transformation",
            "KDEs" : {
                "ProductionDate" : {
                    "Type" : "date",
                    "Name" : "Production Date",
                    "CBVAttribute" : "productionDate",
                    "CSVName" : "ilmd.productionDate",
                    "XPath" : "ilmd/cbvmda:productionDate",
                    "JPath" : "ilmd.cbvmda:productionDate"
                },
                "ExpirationDate" : {
                    "Type" : "date",
                    "Name" : "Expiration Date",
                    "CBVAttribute" : "itemExpirationDate",
                    "CSVName" : "ilmd.itemExpirationDate",
                    "XPath" : "ilmd/cbvmda:itemExpirationDate",
                    "JPath" : "ilmd.cbvmda:itemExpirationDate"
                },
                "CountryOfOrigin" : {
                    "Type" : "string",
                    "Name" : "Country Of Origin",
                    "CBVAttribute" : "countryOfOrigin",
                    "CSVName" : "ilmd.countryOfOrigin",
                    "XPath" : "ilmd/cbvmda:countryOfOrigin",
                    "JPath" : "ilmd.cbvmda:countryOfOrigin"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "ProcessorLicense" : {
                    "Type" : "urn:gdst:cert:processorLicense",
                    "Name" : "Processor License"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "FeedMillEvent" : {
            "ID" : "FeedMillEvent",
            "Name" : "Feed Mill",
            "LocationName" : "Feed Mill",
            "Title" : "Feed Mill",
            "Type" : "InputsAndOutputs",
            "BusinessStep" : "urn:epcglobal:cbv:bizstep:commissioning",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Transformation",
            "KDEs" : {
                "ProteinSource" : {
                    "Type" : "string",
                    "Name" : "Source of Protein",
                    "CBVAttribute" : "proteinSource",
                    "CSVName" : "ilmd.proteinSource",
                    "XPath" : "ilmd/gdst:proteinSource",
                    "JPath" : "ilmd.gdst:proteinSource"
                },
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCertificate" : {
                    "Type" : "urn:gdst:cert:harvestCert",
                    "Name" : "Harvest Certification"
                },
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "HumanWelfarePolicy" : {
                    "Type" : "urn:gdst:cert:humanPolicy",
                    "Name" : "Human Welfare Policy"
                }
            }
        },
        "ReceiveEvent" : {
            "ID" : "ReceiveEvent",
            "Name" : "Receive",
            "LocationName" : "Location",
            "Title" : "Receive",
            "Type" : "References",
            "AllowSSCCs": true,
            "BusinessStep" : "urn:epcglobal:cbv:bizstep:receiving",
            "Disposition" : "active",
            "Action" : "OBSERVE",
            "EPCISClass": "Object",
            "KDEs" : {
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                }
            }
        },
        "ShipEvent" : {
            "ID" : "ShipEvent",
            "Name" : "Ship",
            "LocationName" : "Location",
            "Title" : "Ship",
            "Type" : "References",
            "AllowSSCCs": true,
            "BusinessStep" : "urn:epcglobal:cbv:bizstep:shipping",
            "Disposition" : "active",
            "Action" : "OBSERVE",
            "EPCISClass": "Object",
            "KDEs" : {
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                }
            }
        },
        "DeaggregateEvent" : {
            "ID" : "DeaggregateEvent",
            "Name" : "De-<br/>aggregate",
            "BigTemplateName" : "Deaggregate",
            "LocationName" : "Location",
            "Title" : "Deaggregate",
            "Type" : "Aggregate",
            "BusinessStep" : "urn:epcglobal:cbv:bizstep:unpacking",
            "Action" : "DELETE",
            "EPCISClass": "Aggregate",
            "KDEs" : {
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "AggregatorLicense" : {
                    "Type" : "urn:gdst:cert:aggregatorLicense",
                    "Name" : "Aggregator License"
                }
            }
        },
        "AggregateEvent" : {
            "ID" : "AggregateEvent",
            "Name" : "Aggregate",
            "LocationName" : "Location",
            "Title" : "Aggregate",
            "Type" : "Aggregate",
            "BusinessStep" : "urn:epcglobal:cbv:bizstep:packing",
            "Disposition" : "active",
            "Action" : "ADD",
            "EPCISClass": "Aggregate",
            "KDEs" : {
                "GeoCoordinates" : {
                    "Type" : "coordinates",
                    "Name" : "Geo-Coordiantes",
                    "CBVAttribute" : "readPoint",
                    "CSVName" : "readPoint.id",
                    "XPath" : "readPoint/id",
                    "JPath" : "readPoint.id"
                }
            },
            "Certificates" : {
                "HarvestCoC" : {
                    "Type" : "urn:gdst:cert:harvestCoC",
                    "Name" : "Harvest Chain of Custody Certification"
                },
                "AggregatorLicense" : {
                    "Type" : "urn:gdst:cert:aggregatorLicense",
                    "Name" : "Aggregator License"
                }
            }
        }
    }
}