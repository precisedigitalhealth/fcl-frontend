class GDSTEventEditor {
    constructor(settings) {
        var self = this;

        // assign some internal variables
        self.Element = settings.element;
        self.EventTypes = settings.eventTypes;

        // create a view model
        self.ViewModel = new kendo.observable({
            CTE: null,
            Close: function () {
                self.CloseEditor();
            },

            // grid toolbar buttons
            AddNewReferenceProduct: function () {
                var vm = this;
                vm.CTE.References.push({
                    NetWeight: new GDSTMeasurement(0, "KGM")
                });
            },
            AddNewInputProduct: function () {
                var vm = this;
                vm.CTE.Inputs.push({
                    NetWeight: new GDSTMeasurement(0, "KGM")
                });
            },
            AddNewOutputProduct: function () {
                var vm = this;
                vm.CTE.Outputs.push({
                    NetWeight: new GDSTMeasurement(0, "KGM")
                });
            }
        });
        kendo.bind(self.Element, self.ViewModel);

        // if the user clicks on the background, then we close the editor...
        self.Element.find('.background').on('click', function () {
            self.CloseEditor();
        });

        // initialize the grids
        self.__initialize_grids();
    }


    /** Internal function that finishes setting up the grids.    */
    __initialize_grids() {
        var self = this;
        self.Grids = {};

        // create a resuable editor for the grids
        var productDefEditor = function(container, options) 
        {
            // create an input element
            var input = $("<input/>");
            // set its name to the field to which the column is bound
            input.attr("name", options.field);
            // append it to the container
            input.appendTo(container);
            // initialize a Kendo UI AutoComplete
            input.kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "UUID",
                valuePrimitive: true,
                dataSource: GDSTMasterData.ProductDefinitions.toJSON()
            });
        } 

        // create a resuable editor for the lot numbers
        var lotNumberEditor = function(container, options) {

            // loop through each event and collect a list of potential lot numbers
            var lotNumbers = [];
            var ctes = [self.ViewModel.CTE.toJSON()].concat(GDST.WebApplication.Diagram.KendoDiagram.shapes.map(s => s.dataItem));
            for(var i = 0; i < ctes.length; i++) 
            {
                var cte = ctes[i];
                var products = (cte.References ?? []).concat(cte.Inputs ?? []).concat(cte.Outputs ?? []);
                for (var j = 0; j < products.length; j++) {
                    var product = products[j];
                    if (!GDST.Util.IsStringNullOrEmpty(product.LotNumber)) {
                        if (!lotNumbers.find(l => l === product.LotNumber)) {
                            lotNumbers.push(product.LotNumber);
                        }
                    }
                }
            }

            // create an input element
            var input = $("<input/>");
            // set its name to the field to which the column is bound
            input.attr("name", options.field);
            // append it to the container
            input.appendTo(container);
            // initialize a Kendo UI AutoComplete
            input.kendoComboBox({
                dataSource: lotNumbers
            });
        }

        // create a resuable editor for the lot numbers
        var serialNumberEditor = function(container, options) {

            // loop through each event and collect a list of potential lot numbers
            var serialNumbers = [];
            var ctes = [self.ViewModel.CTE.toJSON()].concat(GDST.WebApplication.Diagram.KendoDiagram.shapes.map(s => s.dataItem));
            for(var i = 0; i < ctes.length; i++) 
            {
                var cte = ctes[i];
                var products = (cte.References ?? []).concat(cte.Inputs ?? []).concat(cte.Outputs ?? []);
                for (var j = 0; j < products.length; j++) {
                    var product = products[j];
                    if (!GDST.Util.IsStringNullOrEmpty(product.SerialNumber)) {
                        if (!serialNumbers.find(l => l === product.SerialNumber)) {
                            serialNumbers.push(product.SerialNumber);
                        }
                    }
                }
            }

            // create an input element
            var input = $("<input/>");
            // set its name to the field to which the column is bound
            input.attr("name", options.field);
            // append it to the container
            input.appendTo(container);
            // initialize a Kendo UI AutoComplete
            input.kendoComboBox({
                dataSource: serialNumbers
            });
        }

        // create a reusable template
        var productDefTemplate = function(data) {
            var name = GDSTMasterData.ProductDefinitions.find(p => p.UUID === data.Product)?.Name ?? "";
            return name;
        }

        // create a resusable editable funciton for lot number
        var lotNumberEditable = function(data) {
            if(!GDST.Util.IsStringNullOrEmpty(data.SerialNumber)) {
                GDST.Util.Alert('Error', `You have already entered a Serial Number 
                                          for this product. Cannot enter both a Serial 
                                        Number and a Lot Number. You must enter one or the other.`);
                return false;
            }
            return true;
        }

        // create a resusable editable funciton for serial number
        var serialNumberEditable = function(data) {
            if(!GDST.Util.IsStringNullOrEmpty(data.LotNumber)) {
                GDST.Util.Alert('Error', `You have already entered a Lot Number 
                                          for this product. Cannot enter both a Serial 
                                        Number and a Lot Number. You must enter one or the other.`);
                return false;
            }
            return true;
        }

        // initialize the refences grid
        self.Grids.ReferencesGrid = self.Element.find('.references-grid').data('kendoGrid');
        self.Grids.ReferencesGrid.setOptions({
            editable: true,
            height: 250,
            columns: [
                { field: "Product", title: "Product Definition", editor: productDefEditor, template: productDefTemplate },
                { field: "LotNumber", title: "Lot Number", editable: lotNumberEditable, editor: lotNumberEditor  },
                //{ field: "SerialNumber", title: "Serial Number", editable: serialNumberEditable, editor: serialNumberEditor },
                {
                    field: "NetWeight",
                    title: "Net Weight",
                    editor: GDST.Kendo.Editors.MeasurementEditor('weight'),
                    template: GDST.Kendo.Templates.MeasurementTemplate('NetWeight')
                },
                {
                    template: `<i class="fa fa-trash icon-button"></i>`,
                    width: 60,
                    attributes: {
                        class: "icon-cell",
                    }
                }
            ],
            dataBound: function (e) {
                var grid = e.sender;

                // bind to the delete button
                $(grid.element).find('.fa-trash').on('click', function () {
                    var data = grid.dataItem($(this).closest('tr'));
                    grid.dataSource.remove(data);
                });
            }
        });

        // initialize the refences grid
        self.Grids.InputsGrid = self.Element.find('.inputs-grid').data('kendoGrid');
        self.Grids.InputsGrid.setOptions({
            editable: true,
            height: 250,
            columns: [
                { field: "Product", title: "Product Definition", editor: productDefEditor, template: productDefTemplate },
                { field: "LotNumber", title: "Lot Number", editable: lotNumberEditable, editor: lotNumberEditor  },
                //{ field: "SerialNumber", title: "Serial Number", editable: serialNumberEditable, editor: serialNumberEditor },
                {
                    field: "NetWeight",
                    title: "Net Weight",
                    editor: GDST.Kendo.Editors.MeasurementEditor('weight'),
                    template: GDST.Kendo.Templates.MeasurementTemplate('NetWeight')
                },
                {
                    template: `<i class="fa fa-trash icon-button"></i>`,
                    width: 60,
                    attributes: {
                        class: "icon-cell",
                    }
                }
            ],
            dataBound: function (e) {
                var grid = e.sender;

                // bind to the delete button
                $(grid.element).find('.fa-trash').on('click', function () {
                    var data = grid.dataItem($(this).closest('tr'));
                    grid.dataSource.remove(data);
                });
            }
        });

        // initialize the outputs grid
        self.Grids.OutputsGrid = self.Element.find('.outputs-grid').data('kendoGrid');
        self.Grids.OutputsGrid.setOptions({
            editable: true,
            height: 250,
            columns: [
                { field: "Product", title: "Product Definition", editor: productDefEditor, template: productDefTemplate },
                { field: "LotNumber", title: "Lot Number", editable: lotNumberEditable, editor: lotNumberEditor  },
                //{ field: "SerialNumber", title: "Serial Number", editable: serialNumberEditable, editor: serialNumberEditor },
                {
                    field: "NetWeight",
                    title: "Net Weight",
                    editor: GDST.Kendo.Editors.MeasurementEditor('weight'),
                    template: GDST.Kendo.Templates.MeasurementTemplate('NetWeight')
                },
                {
                    template: `<i class="fa fa-trash icon-button"></i>`,
                    width: 60,
                    attributes: {
                        class: "icon-cell",
                    }
                }
            ],
            dataBound: function (e) {
                var grid = e.sender;

                // bind to the delete button
                $(grid.element).find('.fa-trash').on('click', function () {
                    var data = grid.dataItem($(this).closest('tr'));
                    grid.dataSource.remove(data);
                });
            }
        });
    }

    /** This triggers a shape to be edited in the CTE Editor. */
    EditCTE(shape) {
        var self = this;    
        console.log(shape.options.dataItem)
        var cte = JSON.parse(JSON.stringify(shape.options.dataItem));
        //console.log(JSON.stringify(shape.options.dataItem))
        cte.EventType = GDST.EventTypes[cte.Type];

        // change the object on the Location, InformationProvider, ProductOwner, Buyer, Seller to the UUID for UI purposes
        cte.Location = cte.latlang?.UUID;
        cte.InformationProvider = cte.InformationProvider?.UUID;
        cte.ProductOwner = cte.ProductOwner?.UUID;
        cte.Buyer = cte.Buyer?.UUID;
        cte.Seller = cte.Seller?.UUID;
        //cte.latlang = cte.latlang?.UUID;

        // change the objects on the Referencees, Information Provider, Product Owner, Buyer, Seller to the UUID for UI purposes
        var fixProductsFunction = function(list) {
            for(var i = 0; i < list.length; i++) {
                list[i].Product = list[i].Product?.UUID;
            }
        }
        fixProductsFunction(cte.References);
        fixProductsFunction(cte.Inputs);
        fixProductsFunction(cte.Outputs);

        // set the CTE into the view model to be edited...
        self.CurrentShape = shape;
        self.ViewModel.set('CTE', cte);

        // build the KDE HTML form
        console.log(cte);

        // initialize the fields for the CTE that is being edited and then bind the fields
        self.__initialize_kdeFields();
        self.__initialize_certificateFields();
        self.__bindFields();

        // display the correct product grids..
        if (self.ViewModel.CTE.EventType.Type.toLowerCase().trim() === 'references') {
            self.Element.find('.references').show();
            self.Element.find('.inputs').hide();
            self.Element.find('.outputs').hide();
            if(self.ViewModel.CTE.EventType.AllowSSCCs === true) {
                self.Element.find('.parentID').show();
            }
            else {
                self.Element.find('.parentID').hide();
            }
        }
        else if (self.ViewModel.CTE.EventType.Type.toLowerCase().trim() === 'aggregate') {
            self.Element.find('.references').hide();
            self.Element.find('.inputs').show();
            self.Element.find('.outputs').hide();
            self.Element.find('.parentID').show();
        }
        else {
            self.Element.find('.references').hide();
            self.Element.find('.inputs').show();
            self.Element.find('.outputs').show();
            self.Element.find('.parentID').hide();
        }

        self.OpenEditor();
    }

    /** This method is used for intializing the certificate fields in the CTE form for a CTE that is being edited. */
    __initialize_certificateFields() {
        var self = this;
        if(!self.ViewModel.CTE.Certificates)
        {
            self.ViewModel.CTE.Certificates = {};
        }
        var certHTML = `<div class="gdst-well">`;
        var hasCertificates = false;
        for(var certProperty in self.ViewModel.CTE.EventType.Certificates)
        {
            var cert = self.ViewModel.CTE.EventType?.Certificates[certProperty];
            if(cert.Type && cert.Type.toLowerCase())
            {
                if(!self.ViewModel.CTE.Certificates[certProperty])
                {
                    self.ViewModel.CTE.Certificates[certProperty] = {
                        Value: '',
                        Standard: ''
                    };
                }
                hasCertificates = true;
                certHTML += `<div class="gdst-certificate-field">
                                <h3>${cert.Name}</h3>
                                <div class="gdst-kde-field">
                                    <label>Standard</label>
                                    <input class="k-input k-textbox" style="width: 100%;"
                                        value="${self.ViewModel.CTE?.Certificates[certProperty]?.Standard ?? ''}"
                                        data-path="Certificates.${certProperty}.Standard"/>
                                </div><div class="gdst-kde-field">
                                    <label>Agency</label>
                                    <input class="k-input k-textbox" style="width: 100%;"
                                        value="${self.ViewModel.CTE?.Certificates[certProperty]?.Agency ?? ''}"
                                        data-path="Certificates.${certProperty}.Agency"/>
                                </div> <div class="gdst-kde-field">
                                    <label>Identification</label>
                                    <input class="k-input k-textbox" style="width: 100%;"
                                        value="${self.ViewModel.CTE?.Certificates[certProperty]?.Identification ?? ''}"
                                        data-path="Certificates.${certProperty}.Identification"/>
                                </div><div class="gdst-kde-field">
                                    <label>Value</label>
                                    <input class="k-input k-textbox" style="width: 100%;"
                                        value="${self.ViewModel.CTE?.Certificates[certProperty]?.Value ?? ''}"
                                        data-path="Certificates.${certProperty}.Value"/>
                                </div>
                            </div>`;
            }
        }
        certHTML += `</div>`;
        self.Element.find('.certificate-fields').html(certHTML);

        // if there are no Certificates, then hide the certificate-fields section
        if (hasCertificates === false) {
            self.Element.find('.certificate-fields').hide();
        }
        else {
            self.Element.find('.certificate-fields').show();
        }
    }

    /** This will initialize the KDE fields for the CTE that is being edited. */
    __initialize_kdeFields() {
        var self = this;
        var locationName = self.ViewModel.CTE.EventType.LocationName;
        self.ViewModel.CTE.GeoCoordinates = [self.ViewModel.CTE.latlng.lat, self.ViewModel.CTE.latlng.lng]
        var kdeHTML = `<div class="gdst-well">
                            <h3>Key Data Elements</h3>
                            <div class="gdst-kde-field">
                                <label>Event Date & Time</label>
                                <input class="gdst-datetime-field" style="width: 100%"
                                       value="${self.ViewModel.CTE.EventTime}"
                                       data-path="EventTime" />
                            </div><div class="gdst-kde-field">
                                <label>Product Owner</label>
                                <input class="product-owner" style="width: 100%" />
                            </div><div class="gdst-kde-field">
                            <label>Information Provider</label>
                                <input class="information-provider" style="width: 100%" />
                            </div><div class="gdst-kde-field">
                                <label>${locationName}</label>
                                <input class="location" style="width: 100%" />
                            </div>`;
        if(self.ViewModel.CTE.EventType.EPCISClass.trim().toLowerCase() === 'object')
        {
            kdeHTML += `<div class="gdst-kde-field">
            <label>Seller (optional)</label>
                <input class="seller" style="width: 100%" />
            </div><div class="gdst-kde-field">
            <label>Buyer (optional)</label>
                <input class="buyer" style="width: 100%" />
            </div>`;
        }
        var kdeCheckboxHTML = ``;
        for (var kdeProperty in self.ViewModel.CTE.EventType.KDEs) {
            var kde = self.ViewModel.CTE.EventType.KDEs[kdeProperty];
            if (kde.Type && kde.Type.toLowerCase) {
                switch (kde.Type.toLowerCase()) {
                    case 'string': {
                        kdeHTML += `<div class="gdst-kde-field">
                                        <label>${kde.Name}</label>
                                        <input class="k-input k-textbox"
                                               value="${self.ViewModel.CTE[kdeProperty] ?? ''}"
                                               data-path="${kdeProperty}"/>
                                    </div>`;
                        break;
                    }
                    case 'dropdownlist' : {
                        kdeHTML += `<div class="gdst-kde-field">
                                        <label>${kde.Name}</label>
                                        <input class="gdst-dropdownlist"
                                               value="${self.ViewModel.CTE[kdeProperty] ?? ''}"
                                               data-path="${kdeProperty}" style="width: 100%" />
                                    </div>`;
                        break;
                    }
                    case 'date': {
                        kdeHTML += `<div class="gdst-kde-field">
                                        <label>${kde.Name}</label>
                                        <input class="gdst-date-field" style="width: 100%;"
                                            value="${self.ViewModel.CTE[kdeProperty] ?? ''}"
                                            data-path="${kdeProperty}"/>
                                    </div>`;
                        break;
                    }
                    case 'boolean': {
                        kdeCheckboxHTML += `<div class="gdst-kde-field">
                                                <input class="k-checkbox" type="checkbox" style="height: 2.0em; width: 2.0em;"
                                                    ${(self.ViewModel.CTE[kdeProperty] === true) ? 'checked' : ''}
                                                    data-path="${kdeProperty}"/>
                                                <label style="font-size: 1.2em; vertical-align: middle;">${kde.Name}</label>
                                            </div>`;
                        break;
                    }
                    case 'coordinates': {
                        kdeHTML += `<div class="gdst-kde-field" style="vertical-align: top;">
                                        <div class="gdst-coordinates-field" style="width: 100%;" data-path="${kdeProperty}">
                                            <div style="display: flex; flex-direction: row;">
                                                <div style="box-sizing: border; width: 50%; padding-right: 0.5 em;">
                                                    <label>Latitude</label>
                                                    <input type="number" class="latitude" step='0.001' style="width: 100%;"
                                                           value="${self.ViewModel.CTE.GeoCoordinates?.[0] ?? 0.0}"
                                                           />
                                                </div>
                                                <div style="box-sizing: border; width: 50%; padding-left: 0.5 em;">
                                                    <label>Longitude</label>
                                                    <input type="number" class="longitude" step='0.001' style="width: 100%;"
                                                           value="${self.ViewModel.CTE.GeoCoordinates?.[1] ?? 0.0}"
                                                           />
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
                        break;
                    }
                }
            }
        }
        kdeHTML += kdeCheckboxHTML;
        kdeHTML += `</div>`;
        self.Element.find('.kde-fields').html(kdeHTML);
    }

    /** This will bind the fields that have been initialized for both KDE and Certificate fields. */
    __bindFields() {
        var self = this;

        // create a trading parner array with a blank option
        var tradingPartners = [{  UUID: "$blank_option$", PGLN: "$blank_option$", Name: "" }].concat(GDSTMasterData.TradingParties.toJSON());
        var locations = [{ UUID: "$blank_option$", GLN: "$blank_option$", Name: "" }].concat(GDSTMasterData.Locations.toJSON());

        // initailize the seller
        self.Element.find('.seller').kendoDropDownList({
            dataSource: tradingPartners,
            dataTextField: 'Name',
            dataValueField: 'UUID',
            dataValuePrimitive: false,
            value: self.ViewModel.CTE.Seller,
            change: function(e) {
                self.ViewModel.CTE.Seller = e.sender.value();
            }
        });

        // initailize the buyer
        self.Element.find('.buyer').kendoDropDownList({
            dataSource: tradingPartners,
            dataTextField: 'Name',
            dataValueField: 'UUID',
            dataValuePrimitive: false,
            value: self.ViewModel.CTE.Buyer,
            change: function(e) {
                self.ViewModel.CTE.Buyer = e.sender.value();
            }
        });

        // initailize the information provider
        self.Element.find('.information-provider').kendoDropDownList({
            dataSource: tradingPartners,
            dataTextField: 'Name',
            dataValueField: 'UUID',
            dataValuePrimitive: false,
            value: self.ViewModel.CTE.InformationProvider,
            change: function(e) {
                self.ViewModel.CTE.InformationProvider = e.sender.value();
            }
        });

        // initailize the product owner
        self.Element.find('.product-owner').kendoDropDownList({
            dataSource: tradingPartners,
            dataTextField: 'Name',
            dataValueField: 'UUID',
            dataValuePrimitive: false,
            value: self.ViewModel.CTE.ProductOwner,
            change: function(e) {
                self.ViewModel.CTE.ProductOwner = e.sender.value();
            }
        });

        // initialize the location
        self.Element.find('.location').kendoDropDownList({
            dataSource: locations,
            dataTextField: 'Name',
            dataValueField: 'UUID',
            dataValuePrimitive: false,
            value: self.ViewModel.CTE.Location,
            change: function(e) {
                self.ViewModel.CTE.Location = e.sender.value();
            }
        });

        // initialize the fields
        // some of the fields are not straight forward text fields, and are kendo widgets
        // se we are going to initialize them here. the internal widget logic will handle
        // the change events
        self.Element.find('.kde-fields .gdst-dropdownlist').each(function () {
            var path = $(this).attr('data-path');
            var originalValue = self.ViewModel.CTE[path];
            var data = [{ Code: "$blank_option$", Name: "" }].concat(self.ViewModel.CTE.EventType.KDEs[path].Options.toJSON());
            $(this).kendoDropDownList({
                value: originalValue,
                dataValueField: 'Code',
                dataTextField: 'Name',
                dataSource: {
                    data: data,
                },
                change: function (e) {
                    var newValue = e.sender.value();
                    var path = e.sender.element.attr('data-path');
                    self.ViewModel.CTE.set(path, newValue);
                }
            });
        });
        self.Element.find('.kde-fields .gdst-date-field').each(function () {
            var originalValue = $(this).val();
            if (originalValue) originalValue = new Date(Date.parse(originalValue));
            $(this).kendoDatePicker({
                value: originalValue,
                change: function (e) {
                    var newValue = e.sender.value();
                    var path = e.sender.element.attr('data-path');
                    self.ViewModel.CTE.set(path, newValue);
                }
            });
        });
        self.Element.find('.kde-fields .gdst-datetime-field').each(function () {
            var originalValue = $(this).val();
            if (originalValue) originalValue = new Date(Date.parse(originalValue));
            $(this).kendoDateTimePicker({
                value: originalValue,
                change: function (e) {
                    var newValue = e.sender.value();
                    var path = e.sender.element.attr('data-path');
                    self.ViewModel.CTE.set(path, newValue);
                }
            });
        });

        // initailize the coordinate fields...
        self.Element.find('.gdst-coordinates-field').each(function () {
            var path = $(this).attr('data-path');
            if (!self.ViewModel.CTE[path]) {
                self.ViewModel.CTE[path] = {
                    Latitude: 0.0,
                    Longitude: 0.0
                };
            }

            // setup the latitude field
            $(this).find('.latitude').kendoNumericTextBox({
                value: self.ViewModel.CTE[path].Latitude,
                change: function (e) {
                    self.ViewModel.CTE[path].Latitude = e.sender.value();
                }
            });

            // setup the latitude field
            $(this).find('.longitude').kendoNumericTextBox({
                value: self.ViewModel.CTE[path].Longitude,
                change: function (e) {
                    self.ViewModel.CTE[path].Longitude = e.sender.value();
                }
            });
        });

        // bind to the checkbox fields
        self.Element.find('.k-checkbox').on('change', function () {
            var checked = $(this).is(':checked');
            var path = $(this).attr('data-path');
            self.ViewModel.CTE.set(path, checked);
        });

        // bind to the text fields
        self.Element.find('.kde-fields .k-input.k-textbox').on('change', function () {
            var newValue = $(this).val();
            var path = $(this).attr('data-path');
            self.ViewModel.CTE.set(path, newValue);
        });

        // bind to the certificate fields
        self.Element.find('.certificate-fields .k-input.k-textbox').on('change', function () {
            var newValue = $(this).val();
            var path = $(this).attr('data-path');
            self.ViewModel.CTE.set(path, newValue);
        });
    }

    /** Makes the CTE Editor visible on the screen. */
    OpenEditor() {
        this.Element.fadeIn(500);
        this.Element.find('#cte-form').removeClass('fadeOutRight');
        this.Element.find('#cte-form').addClass('fadeInRight');
    }

    /** Hides the CTE Editor so that it is no longer visible on the screen. */
    CloseEditor() {
        var self = this;

        // get the cte in JSON format
        var cte = self.ViewModel.CTE.toJSON();

        // convert the Location, Product Owner, Information Provider, Seller, and Buyer to the object form
        var fixLinkedDataFunction = function(prop, list)
        {
            if(cte[prop] && cte[prop] != "$blank_option$") {
                cte[prop] = list.find(l => l.UUID == cte[prop]);
            }
            else {
                cte[prop] = null;
            }
        };
        fixLinkedDataFunction("Location", GDSTMasterData.Locations);
        fixLinkedDataFunction("InformationProvider", GDSTMasterData.TradingParties);
        fixLinkedDataFunction("ProductOwner", GDSTMasterData.TradingParties);
        fixLinkedDataFunction("Buyer", GDSTMasterData.TradingParties);
        fixLinkedDataFunction("Seller", GDSTMasterData.TradingParties);

        // convert the references, inputs, outputs
        var fixGTINFunction = function(list) {
            for(var i = 0; i < list.length; i++) {
                if(list[i].Product && list[i].Product != "$blank_option$")
                {
                    list[i].Product = GDSTMasterData.ProductDefinitions.find(l => l.UUID == list[i].Product);
                }
            }
        }
        fixGTINFunction(cte.References);
        fixGTINFunction(cte.Inputs);
        fixGTINFunction(cte.Outputs);

        // copy the CTE in the view model into the current shape
        /*self.CurrentShape.dataItem = cte;
        self.CurrentShape.redrawVisual();
        GDST.WebApplication.Diagram.RebindShapes();
        GDSTFeedback.CanPerformTraceback(GDST.WebApplication.Diagram.KendoDiagram.shapes, GDST.WebApplication.Diagram.KendoDiagram.connections);*/

        console.log(self.CurrentShape)
        GDST.WebApplication.Diagram.RebindShapes();

        // close the editor
        var self = this;
        self.Element.find('#cte-form').removeClass('fadeInRight');
        self.Element.find('#cte-form').addClass('fadeOutRight');
        self.Element.fadeOut(500);
    }
}