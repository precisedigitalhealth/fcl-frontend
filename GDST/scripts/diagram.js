class GDSTDiagram {

    constructor(settings) {
        var self = this;

        this.Element = settings.element;
        this.EventTypes = settings.eventTypes;
        this.OnDoubleClickItem = settings.onDoubleClickItem;

        // start with the html template
        this.Element.css({
            "display": "flex",
            "flex-direction": "row",
            "height": "100%"
        });

        // var htmlTemplate = ``;
        // this.Element.html(htmlTemplate);

        // setup the feedback tool
        this.Feedback = new GDSTFeedback({
            element: $('#feedback-tool')
        });

        // bind the toolbar view model
        var toolbarVM = new kendo.observable({
            AutoOrganize: function (_e) {
                self.KendoDiagram.layout({
                    type: "layered",
                    subtype: "right"
                });
                self.KendoDiagram.bringIntoView(self.KendoDiagram.shapes);
            },
            Import: function (_e) {
                GDST.WebApplication.ImportTool.Open();
            },
            ImportJSON: function (_e) {
                GDST.WebApplication.ImportJSONTool.Open();
            },
            DownloadPDF: function (_e) {
                var vm = this;
                self.KendoDiagram.saveAsPDF();
            },
            Feedback: function (_e) {
                self.Feedback.Open(self.KendoDiagram);
            },
            MasterData: function(_e) {
                GDST.WebApplication.MasterDataEditor.Open();
            },
            ExportToEPCIS: function (_e) {
                GDSTMasterData.SyncMasterData();
                var csvConverter = new GDSTCSVConverter(self.KendoDiagram.markers, function() {
                    csvConverter.DownloadXML();
                });
            },
            ExportToJSON: function (_e) {
                GDSTMasterData.SyncMasterData();
                var ctes = self.KendoDiagram.shapes.map(s => s.dataItem);
                var epcisDocument = JSON.stringify(GDSTExporter.JSON.Export(ctes), null, 2);
                var contentType = 'text/json;charset=utf-8;';
                var jsonFile = new Blob([epcisDocument], { type: contentType });
                var a = document.createElement('a');
                a.download = 'epcis.json';
                a.href = window.URL.createObjectURL(jsonFile);
                a.textContent = 'Download Json';
                a.dataset.downloadurl = [contentType, a.download, a.href].join(':');
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);

                // double check
                GDST.WebApplication.ImportJSONTool._importJSON(epcisDocument);
                var ctes = GDST.WebApplication.ImportJSONTool.ctes;
                var epcisDocument2 = JSON.stringify(GDSTExporter.JSON.Export(deserializedCTEs), null, 2);
                if (epcisDocument !== epcisDocument2) {
                    jsonFile = new Blob([epcisDocument2], { type: contentType });
                    a = document.createElement('a');
                    a.download = 'epcis-after.json';
                    a.href = window.URL.createObjectURL(jsonFile);
                    a.textContent = 'Download Json';
                    a.dataset.downloadurl = [contentType, a.download, a.href].join(':');
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                }
            },
            ReassignTradingPartyColors: function(e) { 
                GDSTMasterData.GenerateNewTradingPartyColors(e);
            },
        });
        kendo.bind(this.Element.find('.diagram-toolbar'), toolbarVM);

        // initialize the kendo diagram
     /*   this.Element.find('.diagram').kendoDiagram({
            layout: {
                type: "tree"
            },
            connectionDefaults: {
                stroke: {
                    color: "#979797",
                    width: 3
                },
                endCap: "ArrowEnd"
            },
            zoomStart: function (e) {
                // we don't allow double-tap to zoom
                if (e.meta?.type === 'doubleTap') {
                    e.preventDefault();
                }
            },
            shapeDefaults: {
                visual: GDST.Kendo.Diagram.ShapeVisual
            },
            change: function (e) {
                GDSTFeedback.CanPerformTraceback(self.KendoDiagram.shapes, self.KendoDiagram.connections);
            },
            dragEnd: function (e) {
                GDSTFeedback.CanPerformTraceback(self.KendoDiagram.shapes, self.KendoDiagram.connections);
            }
        }); */

        var markers = [];
            // define the layers
            $("#map").kendoMap({
                center: [30.268107, -97.744821],
                zoom: 10,
                layers: [{
                    // map
                    type: "tile",
                    urlTemplate: "https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/#= zoom #/#= y #/#= x #",
                    },{
                    //events and icons
                    type: "marker",
                    dataSource: {
                        transport: {
                            read: function(e) {
                                // Load mock data.
                                e.success(markers);
                            }     
                            // The update method is omitted for simplicity.
                        }
                    },
                    locationField: "latlng",
                    titleField: "name"
                    },{
                    //line and the arrow
                    type: "shape",
                    }],
                markerActivate: function(e) {
                    var marker = e.marker;
                    console.log(marker);
                    var element = marker.element;
                    var width = element.width();
                    var height = element.height();
                    var map = e.sender;
        
                    element.kendoDraggable({
                        // Move the "hot point" of the marker under the cursor.
                        cursorOffset: { top: -height, left: -width / 2 },
        
                        // Create and style the drag hint.
                        hint: function() {
                            var hint = element.clone().css({
                                width: width,
                                height: height,
                                backgroundImage: element.css("backgroundImage")
                            });
        
                            element.hide();
                            return hint;
                        },
        
                        // Constrain the drag area.
                        container: e.layer.map.element,
        
                        // Update the marker location.
                        dragend: function(e) {
                            console.log(e.marker);
                            console.log(marker);
                            e.sender.hint.hide(); 
        
                            var loc = map.viewToLocation([e.x.client, e.y.client]);
                            console.log(loc);
                            console.log(marker.options);
                          /*  marker.options.dataItem = {
                                Type: eventType,
                                References: [],
                                Inputs: [],
                                Outputs: [],
                                EventTime: new Date(),
                                latlng: loc, 
                                name: "latlng",
                            };*/

                            marker.options.dataItem.latlng = loc;
                            marker.options.location = loc;
                        }
                    });
                }
            });
            console.log($("#map").kendoMap);

     /*   var map;
        require(["esri/map"], function(Map) {
        map = new Map("map", {
            basemap: "topo",
            center: [-122.45, 37.75],
            zoom: 13
        });
        });*/

        this.KendoDiagram = $("#map").getKendoMap();
        console.log($("#map").getKendoMap().layers[1].options);
        //set the icons on the map to z index 0
        $("#map").getKendoMap().layers[1].options.zIndex = 0;
        $("#map").getKendoMap().layers[1].options.tooltip.zIndex = 0;
        this.markers = [];

        // load the diagram from the local storage
       /* var loadedData = window.localStorage.getItem('diagram');
        if(loadedData) {
            loadedData = JSON.parse(loadedData);
            var diagramJSON = loadedData.Diagram ?? loadedData;
            if (diagramJSON) {
                self.KendoDiagram.load(diagramJSON);
                toolbarVM.AutoOrganize();
                GDSTFeedback.CanPerformTraceback(self.KendoDiagram.shapes, self.KendoDiagram.connections);
            }

            GDSTMasterData.TradingParties = new kendo.data.ObservableArray(loadedData.TradingParties ?? []);
            GDSTMasterData.ProductDefinitions = new kendo.data.ObservableArray(loadedData.ProductDefinitions ?? []);
            GDSTMasterData.Locations = new kendo.data.ObservableArray(loadedData.Locations ?? []);

            // make sure all trading parties have a color assigned to them
            for(var i = 0; i < GDSTMasterData.TradingParties.length; i++) {
                if(!GDSTMasterData.TradingParties[i].Color) {
                    GDSTMasterData.TradingParties[i].Color = GDST.Util.GenerateRandomColor();
                }
            }
        } 
        this.RebindShapes(); */

        this.RebindShapes();

        // initialize the kendo toolbox and populate it with event types
        this.Element.find('.diagram-toolbox-items .row').html(``);
        for (const property in this.EventTypes) {
            if(this.EventTypes[property].Name) {
                this.Element.find('.diagram-toolbox-items').find('.row').append(`
                <div class="col-xs-6" style="padding: 5px;">
                    <div class="diagram-item" data-event-type="${property}">
                        <div>
                            <label>${this.EventTypes[property].Name}</label>
                        </div>
                    </div>
                </div>
            `);
            }
        }

        // initialize the toolbox items to be draggable
        this.Element.find('.diagram-item').kendoDraggable({
            hint: function (e) {
                return $(e[0]).clone();
            },
        });

        

        // intialize the drop target
        this.Element.find('.diagram-container').kendoDropTarget({

            // this event is triggered when a diagram item is dragged into the diagram container from the toolbox
            drop: function (e) {

                // we are going to add a shape into the kendo diagram at the X and Y coordinates where the 
                // shape was dropped inside the diagram container.
                console.log(e);
                console.log(e.marker);
                var diagramItemEle = $(e.draggable.currentTarget);
                console.log(diagramItemEle);
                var eventType = $(diagramItemEle).attr('data-event-type');
                console.log(eventType);
                if(eventType != undefined)
                {
                    var label = self.EventTypes[eventType].Name;
                    var offset = self.Element.find('.diagram').offset();
                    offset = {top: 105, left: 210};
                    var map = $("#map").getKendoMap();
                    var marker = map.marker;

                    var shape = marker;
                    if(eventType === "FishingEvent")
                    {
                        shape = "Fishing";
                    }
                    if(eventType === "FeedMillEvent")
                    {
                        shape = "Feed";
                    }
                    if(eventType === "FarmHarvestEvent")
                    {
                        shape = "Farm";
                    }
                    if(eventType === "AggregateEvent")
                    {
                        shape = "Aggregate";
                    }
                    if(eventType === "ProcessingEvent")
                    {
                        shape = "Processing";
                    }
                    if(eventType === "ShipEvent")
                    {
                        shape = "ship";
                    }
                    if(eventType === "TransshipEvent")
                    {
                        shape = "Transship";
                    }

                    //self.KendoDiagram.addShape(shape);
                    var loc = map.viewToLocation([500, 300]);
                //  marker.dataItem.set("latlng", [loc.lat, loc.lng], "name", eventType);
                    console.log(self.EventTypes[eventType]);
                    console.log(label);
                    map.bind("click", function(e) {
                        if(eventType != undefined)
                        {
                            console.log(e.location);
                            e.sender.markers.add({
                            location: e.location,
                            tooltip:{
                                    content:function(e){                       
                                    var content = label;
                                    content += "<button data-location='" + e.sender.marker.location();
                                    return content += "' style=' margin-bottom: 0;'"
                                    },
                                    showOn: "click",
                                    show:function(e){
                                        self.onShow(e);
                                    }
                                },
                            shape: shape,
                           // shape: diagramItemEle[0].outerHTML,
                            dataItem: {
                                Type: eventType,
                                References: [],
                                Inputs: [],
                                Outputs: [],
                                EventTime: new Date(),
                                Location: [e.location.latlng],
                                latlng: e.location, 
                                name: label,
                                EventType: self.EventTypes[eventType]
                            },
                        });
                      //  markers.push({"latlng":[500, 300], "name": label});
                        eventType = undefined;
                       } 
                    });
               }
              /*  else
                {
                    console.log($("#map").getKendoMap().dataItem);
                    $("#map").getKendoMap().markerActivate;
                }*/
                self.RebindShapes();
            }
        });

        // initialize the auto-save
        var interval = setInterval(function() {
            self.Save();
        }, 500);
    }

    Save() {
        var self = this;

        //uncomment this to prevent saving
        return;

        // convert the shapes and connections to JSON
        var json = self.KendoDiagram.save();
        var shapes = self.KendoDiagram.shapes;
        for (var i = 0; i < json.shapes.length; i++) {
            json.shapes[i].dataItem = shapes.find(s => s.id == json.shapes[i].id).dataItem;
        }

        var saveData = {
            TradingParties: GDSTMasterData.TradingParties.toJSON(),
            ProductDefinitions: GDSTMasterData.ProductDefinitions.toJSON(),
            Locations: GDSTMasterData.Locations.toJSON(),
            Diagram: json
        };

        // save it into the local storage
        window.localStorage.setItem('diagram', JSON.stringify(saveData));
    }

    /** Binds the shapres in the diagram. This should be called each time shapes are removed or added to the diagram. */
   /* RebindShapes() {
        var self = this;
        var shapes = self.KendoDiagram.shapes;
        for (var i = 0; i < shapes.length; i++) {
            var shape = shapes[i];
            var ele = $(shape.shapeVisual.drawingElement._observers[0].element);
            ele.off('dblclick');
            ele.attr('data-id', shape.id);
            ele.on('dblclick', function (e) {
                var id = $(this).attr('data-id');
                var shape = self.KendoDiagram.getShapeById(id);
                self.OnDoubleClickItem(shape);
                e.preventDefault();
            });
        }
    } */

    RebindShapes() {
        var self = this;
        var markers = $("#map").getKendoMap().markers;
        for (var i = 0; i < markers.length; i++) {
            var marker = markers[i];
            var ele = $(marker.shapeVisual.drawingElement._observers[0].element);
            console.log(ele);
            ele.off('dblclick');
            ele.attr('data-id', marker.id);
            ele.on('dblclick', function (e) {
                var id = $(this).attr('data-id');
                var marker = self.KendoDiagram.getShapeById(id);
                self.OnDoubleClickItem(marker);
                e.preventDefault();   
            });
        }
    }

    onShow(e)
    {
        var self = this;
        console.log("test");
        console.log(e);
        var map = $("#map").getKendoMap();
        var geom = kendo.geometry;

      //  var from = [20.69, -70.96];
       // var to = [18.89, -72.19];
        console.log(self.markers);
        console.log(self.markers.length);
       // console.log(self.markers[0].location());

        if(self.markers.length == 1)
        {
            var from = map.locationToView(self.markers[0].location());
            var to = map.locationToView(e.sender.marker.location());
            console.log("to is here")
            console.log(to)

            const triangle_width = 10
            console.log(triangle_width)
            var shapeLayer = map.layers[2];

            // var group = new kendo.dataviz.drawing.Group()
            var line = new kendo.dataviz.drawing.Path({
            stroke: {
                color: "#FF0000",
                width: 3,
                lineCap: "round"
            }
            });
            line.moveTo(from).lineTo(to);
            // group.append(line)
            
            const delta_x = to.x - from.x
            const delta_y = from.y - to.y
            var angleDeg = Math.atan2(delta_y, delta_x) * 180 / Math.PI;

            //draw the arrow
            const arrow = new kendo.dataviz.drawing.Path({
                stroke: {
                    color: "#FF0000",
                    width: 3,
                    lineCap: "round"
                },
                fill: {
                    color: "#FF0000"
                    },
            });
            arrow.moveTo(to).lineTo(to.x + triangle_width, to.y).lineTo(to.x + 0.5 * triangle_width, to.y + triangle_width).close();
            arrow.moveTo(to.x - triangle_width, to.y + 0.5* triangle_width).lineTo(to.x - triangle_width, to.y - 0.5* triangle_width).lineTo(to).close();
            arrow.transform(geom.transform().rotate(-1*angleDeg, to));
            shapeLayer.surface.draw(line);
            shapeLayer.surface.draw(arrow);
            console.log("draw the arrow")
            self.markers = [];
            console.log(shapeLayer);
           // GDSTFeedback.CanPerformTraceback(self.markers, self.shapeLayer..connections);
        }
        else{
            self.markers.push(e.sender.marker);
            console.log(self.markers);
        }

        self.OnDoubleClickItem(e.sender.marker);

      //  var to = map.locationToView(data.pointTo);
        e.preventDefault();
    }
}

