class GLNEditor {

    _html = `
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <label>GLN Type</label>
                    <div data-role="dropdownlist" 
                        data-bind="value: SelectedGLNType, source: GLNTypes, events: { change: OnGLNTypeChanged }"
                        style="width: 100%;"></div>
                </div>
            </div>
            <div class="row" style="margin-top: 5px;">
                <div class="col-xs-12 text-center">
                    <label>Company Prefix</label>
                    <input class="k-input k-textbox" data-bind="value: GLN.Prefix" style="width: 100%; border: 1px solid black;" />
                </div>
            </div>
            <div class="row" style="margin-top: 5px;">
                <div class="col-xs-12 text-center">
                    <label>Serial Number</label>
                    <input class="k-input k-textbox" data-bind="value: GLN.SerialNumber" style="width: 100%; border: 1px solid black;" />
                </div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-xs-12">
                    <div class="errors" style="color: red;">

                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-xs-12 text-center">
                    <center>
                        <button class="gdst-button" data-bind="click: Save">
                            <i class="fa fa-save" style="margin-right: 5px;" />
                            <span>Save</span>
                        </button>
                    </center>
                </div>
            </div>
        </div>
    `;

    constructor() {
        var that = this;

        // add the html to the index.html
        var ele = $(this._html);
        ele.appendTo("#windows");

        // initialize the window
        this._window = $(ele).kendoWindow({
            visible: false,
            width: "550px",
            title: 'GLN Builder'
        }).data('kendoWindow');

        // initialize the viewmodel
        this._vm = new kendo.observable({
            GLN: null, 
            GLNTypes: ["GS1", "Non-GS1"],
           SelectedGLNType: "GS1",
           ShowIndicatorDigit: true,
           OnGLNTypeChanged: function(e) {
               this.set('ShowIndicatorDigit', (this.SelectedGLNType === "GS1"));
           },
           Save: function (e) {
                var glnStr = '';
                var errors = [];
                switch (this.SelectedGLNType) {
                    case "GS1" : {
                        var regex = /^\d+$/;

                        // prefix checks
                        if (this.GLN.Prefix.length < 1) errors.push('Prefix is not filled in. GS1 GLNs require a Prefix.');
                        else if (!regex.test(this.GLN.Prefix)) errors.push('Prefix can only be digits.');

                        // serial number checks
                        if (this.GLN.SerialNumber.length < 1) errors.push('Serial Number is not filled in. GS1 GLNs require a Serial Number.');
                        else if (!regex.test(this.GLN.SerialNumber)) errors.push('Serial Number can only be digits.');

                        // check the total length
                        var glnLength = (this.GLN.Prefix + this.GLN.SerialNumber).length;
                        if (glnLength !== 12) {
                            errors.push('The Prefix and Serial Number should add up to a length of 12. ' + 
                                        'They add up to a length of ' + glnLength + '.');
                        }

                        // build the GLN
                        glnStr = `urn:epc:id:sgln:${this.GLN.Prefix}.${this.GLN.SerialNumber}`; 
                    }
                    break;
                    case "Non-GS1" :{

                        // prefix checks
                        if (this.GLN.Prefix.length < 1) errors.push('Prefix is not filled in. Non-GS1 GLNs require a Prefix.');

                        // serial number checks
                        if (this.GLN.SerialNumber.length < 1) errors.push('Serial Number is not filled in. Non-GS1 GLNs require a Serial Number.');

                        // build the GLN
                        if (this.GLN.IsPGLN) {
                            glnStr = `urn:gdst:example.org:party:${this.GLN.Prefix}.${this.GLN.SerialNumber}`; 
                        }
                        else {
                            glnStr = `urn:gdst:example.org:location:loc:${this.GLN.Prefix}.${this.GLN.SerialNumber}`; 
                        }
                    }
                    break;
                }

                // if there were any errors, then report the errors
                if (errors.length > 0) {
                    var errorsHTML = `<ul>`;
                    for(var i = 0; i < errors.length; i++){
                        errorsHTML += `<li>${errors[i]}</li>`;
                    }
                    errorsHTML += `</ul>`;
                    that._window.element.find('.errors').html(errorsHTML);
                    that._window.center();
                }
                // otherwise callback with the GLN
                else {
                    that._window.element.find('.errors').html('');
                    that._window.center();
                    that._window.close();
                    if(this.Callback) this.Callback(glnStr);
                }
           }
        });

        // bind the view model
        kendo.bind(ele, this._vm);
    }

    /** Create a GLN. */
    CreateGLN (callback, isPGLN) {
        this._vm.set('GLN', {
            IsPGLN: (isPGLN) ? true : false,
            Prefix: this._vm.GTIN?.Prefix,
            SerialNumber: '',
        });
        this._vm.set('Callback', callback);
        this._window.element.find('.errors').html('');
        this._window.center().open();
    }
}