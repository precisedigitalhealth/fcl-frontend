class GTINEditor {

    _html = `
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <label>GTIN Type</label>
                    <div data-role="dropdownlist" 
                        data-bind="value: SelectedGTINType, source: GTINTypes, events: { change: OnGTINTypeChanged }"
                        style="width: 100%;"></div>
                </div>
            </div>
            <div class="row" style="margin-top: 5px;" data-bind="visible: ShowIndicatorDigit">
                <div class="col-xs-12 text-center">
                    <label>Indicator Digit</label>
                    <input class="k-input k-textbox" data-bind="value: GTIN.IndicatorDigit" style="width: 100%; border: 1px solid black;" />
                </div>
            </div>
            <div class="row" style="margin-top: 5px;">
                <div class="col-xs-12 text-center">
                    <label>Company Prefix</label>
                    <input class="k-input k-textbox" data-bind="value: GTIN.Prefix" style="width: 100%; border: 1px solid black;" />
                </div>
            </div>
            <div class="row" style="margin-top: 5px;">
                <div class="col-xs-12 text-center">
                    <label>Serial Number</label>
                    <input class="k-input k-textbox" data-bind="value: GTIN.SerialNumber" style="width: 100%; border: 1px solid black;" />
                </div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-xs-12">
                    <div class="errors" style="color: red;">

                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-xs-12 text-center">
                    <center>
                        <button class="gdst-button" data-bind="click: Save">
                            <i class="fa fa-save" style="margin-right: 5px;" />
                            <span>Save</span>
                        </button>
                    </center>
                </div>
            </div>
        </div>
    `;

    constructor() {
        var that = this;

        // add the html to the index.html
        var ele = $(this._html);
        ele.appendTo("#windows");

        // initialize the window
        this._window = $(ele).kendoWindow({
            visible: false,
            width: "550px",
            title: 'GTIN Builder'
        }).data('kendoWindow');

        // initialize the viewmodel
        this._vm = new kendo.observable({
           GTIN: null, 
           GTINTypes: ["GS1", "Non-GS1"],
           SelectedGTINType: "GS1",
           ShowIndicatorDigit: true,
           OnGTINTypeChanged: function(e) {
               this.set('ShowIndicatorDigit', (this.SelectedGTINType === "GS1"));
           },
           Save: function (e) {
                var gtinStr = '';
                var errors = [];
                switch (this.SelectedGTINType) {
                    case "GS1" : {
                        var regex = /^\d+$/;

                        // indicator digit checks
                        if (this.GTIN.IndicatorDigit.length < 1) errors.push('Indicator Digit is not filled in. GS1 GTINs require an Indicator Digit.');
                        else if (this.GTIN.IndicatorDigit.length !== 1) errors.push('Indicator Digit cannot be longer than one digit.');
                        else if (!regex.test(this.GTIN.IndicatorDigit)) errors.push('Indicator Digit must be a single digit of 0 through 9.');

                        // prefix checks
                        if (this.GTIN.Prefix.length < 1) errors.push('Prefix is not filled in. GS1 GTINs require a Prefix.');
                        else if (!regex.test(this.GTIN.Prefix)) errors.push('Prefix can only be digits.');

                        // serial number checks
                        if (this.GTIN.SerialNumber.length < 1) errors.push('Serial Number is not filled in. GS1 GTINs require a Serial Number.');
                        else if (!regex.test(this.GTIN.SerialNumber)) errors.push('Serial Number can only be digits.');

                        // check the total length
                        var gtinLength = (this.GTIN.IndicatorDigit + this.GTIN.Prefix + this.GTIN.SerialNumber).length;
                        if (gtinLength !== 13) {
                            errors.push('The Indicator Digit, Prefix, and Serial Number should add up to a length of 13. ' + 
                                        'They add up to a length of ' + gtinLength + '.');
                        }

                        // build the GTIN
                        gtinStr = `urn:epc:idpat:sgtin:${this.GTIN.Prefix}.${this.GTIN.IndicatorDigit}${this.GTIN.SerialNumber}`; 
                    }
                    break;
                    case "Non-GS1" :{

                        // prefix checks
                        if (this.GTIN.Prefix.length < 1) errors.push('Prefix is not filled in. Non-GS1 GTINs require a Prefix.');

                        // serial number checks
                        if (this.GTIN.SerialNumber.length < 1) errors.push('Serial Number is not filled in. Non-GS1 GTINs require a Serial Number.');

                        // build the GTIN
                        gtinStr = `urn:gdst:example.org:product:class:${this.GTIN.Prefix}.${this.GTIN.SerialNumber}`; 
                    }
                    break;
                }

                // if there were any errors, then report the errors
                if (errors.length > 0) {
                    var errorsHTML = `<ul>`;
                    for(var i = 0; i < errors.length; i++){
                        errorsHTML += `<li>${errors[i]}</li>`;
                    }
                    errorsHTML += `</ul>`;
                    that._window.element.find('.errors').html(errorsHTML);
                    that._window.center();
                }
                // otherwise callback with the GTIN
                else {
                    that._window.element.find('.errors').html('');
                    that._window.center();
                    that._window.close();
                    if(this.Callback) this.Callback(gtinStr);
                }
           }
        });

        // bind the view model
        kendo.bind(ele, this._vm);
    }

    /** Create a GTIN. */
    CreateGTIN (callback) {
        this._vm.set('GTIN', {
            IndicatorDigit: '',
            Prefix: this._vm.GTIN?.Prefix,
            SerialNumber: '',
        });
        this._vm.set('Callback', callback);
        this._window.element.find('.errors').html('');
        this._window.center().open();
    }
}