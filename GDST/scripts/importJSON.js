class GDSTJsonImporter {

    constructor(element) {
        var self = this;

        this._window = $(element).kendoWindow({
            visible: false,
            width: "600px",
            title: 'EPCIS JSON Import'
        }).data('kendoWindow');


        this._vm = new kendo.observable({
            Import: function (e) {
                var upload = $("#importJSON").data('kendoUpload');
                if (upload.getFiles().length > 0) {
                    var files = upload.getFiles();
                    for (var i = 0; i < files.length; i++) {
                        var selectedFile = files[i];
                        const reader = new FileReader();
                        reader.addEventListener('load', (event) => {
                            var text = event.target.result;
                            self._importJSON(text);
                            self._window.close();
                        });
                        reader.readAsText(selectedFile.rawFile);
                    }
                }
                else {
                    GDST.Util.Alert('Error', 'No file selected.');
                }
            }
        });
        kendo.bind(element, this._vm);
    }

    Open() {
        this._window.center().open();
    }

    _importJSON(json) {
        json = JSON.parse(json);
        this._readProductDefinitions(json);
        this._readLocations(json);
        this._readTradingParties(json);
        this._readCTEs(json);

        var shapes = this._convertEventsToShapes(this.ctes);

        // load the data into the web application
        GDSTMasterData.TradingParties = new kendo.data.ObservableArray(this.tradingParties ?? []);
        GDSTMasterData.ProductDefinitions = new kendo.data.ObservableArray(this.productDefs ?? []);
        GDSTMasterData.Locations = new kendo.data.ObservableArray(this.locations ?? []);
        GDST.WebApplication.MasterDataEditor._vm.set('TradingParties', GDSTMasterData.TradingParties);
        GDST.WebApplication.MasterDataEditor._vm.set('ProductDefinitions', GDSTMasterData.ProductDefinitions);
        GDST.WebApplication.MasterDataEditor._vm.set('Locations', GDSTMasterData.Locations);
        GDST.WebApplication.Diagram.KendoDiagram.clear();
        for (var i = 0; i < shapes.length; i++) {
            GDST.WebApplication.Diagram.KendoDiagram.addShape(shapes[i]);
        }
        GDST.WebApplication.Diagram.RebindShapes();

        // generate all the connections...
        var connectionsMap = new Map();
        var connections = [];
        var determineIfLinkExists = function(list, pi) {
            for(var i = 0; i < list.length; i++) {
                if(list[i].Product?.GTIN && list[i].LotNumber) {
                    if(list[i].Product?.GTIN === pi.Product?.GTIN && list[i].LotNumber === pi.LotNumber) {
                        return true;
                    }
                }
            }
            return false;
        }
        var findClosestEvent = function (shapes, currentShape, currentPI, filter) {

            // filter the shapes to only events that occured beefore the currentShape...
            var filteredShapes = shapes.filter(s => Date.parse(s.dataItem.EventTime) < Date.parse(currentShape.dataItem.EventTime))
            filteredShapes = filteredShapes.sort((a,b) =>  {
                var dParseA = Date.parse(a.dataItem.EventTime);
                var dParseB  = Date.parse(b.dataItem.EventTime);
                if(dParseA === dParseB) {
                    return 0;
                }
                else if(dParseA > dParseB) {
                    return 1;
                }
                else {
                    return -1;
                }
            });

            // find the event that comes right before this one where the filter returns true
            // cannot return the same event as the current event
            var date = currentShape.dataItem.EventTime;
            date = (date instanceof Date) ? date : new Date(Date.parse(date));
            var matchShape = null;
            var matchDate = null;
            for (var i = 0; i < filteredShapes.length; i++) {
                var s = filteredShapes[i];
                var d = s.dataItem.EventTime;
                d = (d instanceof Date) ? d : new Date(Date.parse(d));
                if (d.getTime() < date.getTime()) {
                    var filterResult = filter(s.dataItem, currentPI);
                    if(filterResult === 'stop') {
                        break;
                    }
                    if (filterResult === true) {
                        if (matchDate && matchShape) {
                            if (matchDate.getTime() < d.getTime()) {
                                matchShape = s;
                                matchDate = d;
                            }
                        }
                        else {
                            matchShape = s;
                            matchDate = d;
                        }
                    }
                }
            }

            // return null if nothing found that meets the filter with an event time before the current event
            return matchShape;
        }

        // sort the events by their event time...
        var connections = [];
        for (var i = 0; i < GDST.WebApplication.Diagram.KendoDiagram.shapes.length; i++) {
            var shape = GDST.WebApplication.Diagram.KendoDiagram.shapes[i];
            var cte = shape.dataItem;

            // foreach input...
            // find the event with the closest event time that is before the current event where the 
            // input is either in the outputs, references, or the inputs of an disaggregate event
            // if we find this event, then create a connection between that shape and this shape (target).
            var inputRefFilter = function (c, pi) {
                if (c.EventType.ID === 'DeaggregateEvent' && determineIfLinkExists(c.Inputs, pi)) {
                    return true;
                }
                else if (determineIfLinkExists(c.References, pi)) {
                    return true;
                }
                else if (determineIfLinkExists(c.Outputs, pi)) {
                    return true;
                }
                return false;
            };

            // if we have a Parent ID, and we are not an Aggregate event, then look for Ship, Receive, or Disaggregate
            // with the closest event time that is before the current event where the ParentID matches this event's 
            // parent ID.
            var parentFilter = function (c) {
                if (c.ParentID === cte.ParentID) {
                    return true;
                }
                else if(c.Type === 'AggregateEvent') {
                    return 'stop';
                }
                return false;
            }
            
            if (cte.Type === 'AggregateEvent' || GDST.Util.IsStringNullOrEmpty(cte.ParentID)) {
                for (var j = 0; j < cte.References.length; j++) {
                    var matchingShape = findClosestEvent(GDST.WebApplication.Diagram.KendoDiagram.shapes, shape, cte.References[j], inputRefFilter);
                    if (matchingShape) {
                        connections.push({ from: matchingShape, to: shape});
                    }
                }
                for (var j = 0; j < cte.Inputs.length; j++) {
                    var matchingShape = findClosestEvent(GDST.WebApplication.Diagram.KendoDiagram.shapes, shape, cte.Inputs[j], inputRefFilter);
                    if (matchingShape) {
                        connections.push({ from: matchingShape, to: shape});
                    }
                }
            }
            else {
                var matchingShape = findClosestEvent(GDST.WebApplication.Diagram.KendoDiagram.shapes, shape, cte, parentFilter);
                if (matchingShape) {
                    connections.push({ from: matchingShape, to: shape});
                }
            }
        }

        // remove duplicate connections
        var uniqueConnections = [];
        for (var i = 0; i < connections.length; i++) {
            var conn = connections[i];
            if(uniqueConnections.find(c => c.to.id === conn.to.id && c.from.id === conn.from.id) === undefined)
            {
                uniqueConnections.push(conn);
            }
        }

        // add the connections
        for (var i = 0; i < uniqueConnections.length; i++) {
            var connection = new kendo.dataviz.diagram.Connection(uniqueConnections[i].from, uniqueConnections[i].to);
            connection.from.shape = uniqueConnections[i].from;
            connection.to.shape = uniqueConnections[i].to;
            GDST.WebApplication.Diagram.KendoDiagram.addConnection(connection);
        }

        // refresh some stuff
        GDST.WebApplication.Diagram.KendoDiagram.layout({
            type: "layered",
            subtype: "right"
        });
        GDST.WebApplication.Diagram.KendoDiagram.bringIntoView(GDST.WebApplication.Diagram.KendoDiagram.shapes);
        GDSTMasterData.GenerateNewTradingPartyColors();
        GDSTFeedback.CanPerformTraceback(GDST.WebApplication.Diagram.KendoDiagram.shapes, GDST.WebApplication.Diagram.KendoDiagram.connections);
    }

    _readProductDefinitions(json) {
        this.productDefs = [];
        var pDefs = json.epcisBody.vocabularyList.find(v => v.type === 'urn:epcglobal:epcis:vtype:EPCClass')?.vocabularyElementList ?? [];
        for (var i = 0; i < pDefs.length; i++) {
            var pDef = {
                GTIN: pDefs[i]["id"],
                Name: pDefs[i]["cbvmda:descriptionShort"],
                ProductForm: pDefs[i]["cbvmda:additionalTradeItemIdentification"],
                Species: pDefs[i]["cbvmda:speciesForFisheryStatisticsPurposesName"]
            };
            pDef.UUID = GDST.Util.GenerateUUID();
            this.productDefs.push(pDef);
        }
    }

    _readLocations(json) {
        this.locations = [];
        var locs = json.epcisBody.vocabularyList.find(v => v.type === 'urn:epcglobal:epcis:vtype:Location')?.vocabularyElementList ?? [];
        for (var i = 0; i < locs.length; i++) {
            var loc = {
                GLN: locs[i]["id"],
                Name: locs[i]["cbvmda:name"],
                Address1: locs[i]["cbvmda:streetAddressOne"],
                Address2: locs[1]["cbvmda:streetAddressTwo"],
                City: locs[i]["cbvmda:city"],
                State: locs[i]["cbvmda:state"],
                Country: locs[i]["cbvmda:countryCode"],
                VesselID: locs[i]["cbvmda:vesselID"],
                IMONumber: locs[i]["cbvmda:imoNumber"],
                VesselFlag: locs[i]["cbvmda:vesselFlagState"],
            };
            loc.UUID = GDST.Util.GenerateUUID();
            this.locations.push(loc);
        }
    }

    _readTradingParties(json) {
        this.tradingParties = [];
        var tps = json.epcisBody.vocabularyList.find(v => v.type === 'urn:epcglobal:epcis:vtype:TradingParty')?.vocabularyElementList ?? [];
        for (var i = 0; i < tps.length; i++) {
            var tp = {
                PGLN: tps[i]["id"],
                Name: tps[i]["cbvmda:name"]
            };
            tp.UUID = GDST.Util.GenerateUUID();
            this.tradingParties.push(tp);
        }
    }

    _lookUpLocation(gln) {
        var location = this.locations.find(l => l.GLN === gln);
        return location;
    }

    _lookUpTradingParty(pgln) {
        var tp = this.tradingParties.find(t => t.PGLN === pgln);
        return tp;
    }

    _lookUpProductDefinition(gtin) {
        var productDef = this.productDefs.find(p => p.GTIN === gtin);
        return productDef;
    }

    _readCTEs(json) {
        this.ctes = [];
        var epcisCTEs = json.epcisBody.eventList;
        for (var i = 0; i < epcisCTEs.length; i++) {
            var epcisCTE = epcisCTEs[i];
            var cte = {};
            cte.Guid = epcisCTE.id;
            cte.EventTime = new Date(Date.parse(epcisCTE.eventTime));
            cte.EventType = GDST.EventTypes.FindByBizStep(epcisCTE.bizStep);
            cte.Type = cte.EventType.ID;
            cte.Location = this._lookUpLocation(epcisCTE.bizLocation?.id);
            cte.ProductOwner = this._lookUpTradingParty(epcisCTE["gdst:productOwner"]);
            cte.InformationProvider = this._lookUpTradingParty(epcisCTE["cbvmda:informationProvider"]);
            cte.Inputs = [];
            cte.References = [];
            cte.Outputs = [];

            // destinations
            for (var j = 0; j < epcisCTE.destinationList.length; j++) {
                var dest = epcisCTE.destinationList[j];
                if(dest.type === "urn:epcglobal:cbv:sdt:owning_party"){
                    cte.Buyer = this._lookUpTradingParty(dest.destination);
                }
            }

            // sources
            for (var j = 0; j < epcisCTE.sourceList.length; j++) {
                var source = epcisCTE.sourceList[j];
                if(source.type === "urn:epcglobal:cbv:sdt:owning_party"){
                    cte.Seller = this._lookUpTradingParty(source.source);
                }
            }

            // object events
            if (epcisCTE.isA.toLowerCase() === "object") {

                // try to capture the SCCC from the epc list
                if (epcisCTE.epcList.find(e => e.startsWith('urn:sscc:')) !== undefined) {
                    cte.ParentID = epcisCTE.epcList.find(e => e.startsWith('urn:sscc:'));
                }

                // quantity list
                if (epcisCTE.quantityList) {
                    for (var j = 0; j < epcisCTE.quantityList.length; j++) {
                        var item = epcisCTE.quantityList[j];
                        var piRef = this._buildProductFromQuantityList(item);
                        cte.References.push(piRef);
                    }
                }
            }
            // transformation event
            else if (epcisCTE.isA.toLowerCase() === "transformation") {

                // input quantity list
                if (epcisCTE.inputQuantityList) {
                    for (var j = 0; j < epcisCTE.inputQuantityList.length; j++) {
                        var item = epcisCTE.inputQuantityList[j];
                        var piRef = this._buildProductFromQuantityList(item);
                        cte.Inputs.push(piRef);
                    }
                }

                // output quantity list
                if (epcisCTE.outputQuantityList) {
                    for (var j = 0; j < epcisCTE.outputQuantityList.length; j++) {
                        var item = epcisCTE.outputQuantityList[j];
                        var piRef = this._buildProductFromQuantityList(item);
                        cte.Outputs.push(piRef);
                    }
                }
            }
            // aggregate event
            else if (epcisCTE.isA.toLowerCase() === "aggregate") {

                // parent ID
                cte.ParentID = epcisCTE.parentID;

                // child quantity list
                if (epcisCTE.childQuantityList) {
                    for (var j = 0; j < epcisCTE.childQuantityList.length; j++) {
                        var item = epcisCTE.childQuantityList[j];
                        var piRef = this._buildProductFromQuantityList(item);
                        cte.Inputs.push(piRef);
                    }
                }
            }

            // kdes
            for (var prop in cte.EventType.KDEs) {
                var jpath = cte.EventType.KDEs[prop].JPath;
                var datatype = cte.EventType.KDEs[prop].Type;
                var value = this._getValue(epcisCTE, jpath);
                if (datatype == "coordinates") {
                    var pieces = value.split(':');
                    if( pieces.length > 1) {
                        var pieces2 = pieces[1].split(',');
                        if (pieces2.length > 1){
                            value = {
                                Latitude: parseFloat(pieces2[0]),
                                Longitude: parseFloat(pieces2[1])
                            };
                        }
                    }
                }
                cte[prop] = value;
            }

            // certificates
            cte.Certificates = {};
            if (epcisCTE["cbvmda:certificationList"]) {
                for(var m = 0; m < epcisCTE["cbvmda:certificationList"].length; m++) {
                    var gs1Certificate = epcisCTE["cbvmda:certificationList"][m];
                    var cert = {
                        Type: gs1Certificate["gdst:certificationType"],
                        Standard: gs1Certificate["cbvmda:certificationStandard"],
                        Agency: gs1Certificate["cbvmda:certificationStandard"],
                        Value: gs1Certificate["cbvmda:certificationValue"],
                        Identification: gs1Certificate["cbvmda:certificationIdentification"]
                    }
                    for (var prop in cte.EventType.Certificates) {
                        var certType = cte.EventType.Certificates[prop];
                        if(certType.Type === cert.Type) {
                            cte.Certificates[prop] = cert;
                        }
                    }
                }
            }

            this.ctes.push(cte);
        }
    }

    _convertEventsToShapes(events) {
        var shapes = [];
        for (var i = 0; i < events.length; i++) {
            shapes.push({
                dataItem: events[i],
                x: 0,
                y: 0
            });
        }
        return shapes;
    }

    _getValue(object, jpath) {

        if (!object) return null;

        if (GDST.Util.IsArray(object)) {
            if (object.length < 1) return null;
            else {
                object = object[0];
            }
        }

        if (!object) return null;

        var pathParts = jpath.split('.');
        if (pathParts.length === 1) {
            return object[jpath];
        }
        else {
            var newObject = object[pathParts[0]];
            if (newObject) {
                var newJPath = "";
                for (var i = 1; i < pathParts.length; i++) {
                    if (i > 1) { newJPath += "."; }
                    newJPath += pathParts[i];
                }
                return this._getValue(newObject, newJPath);
            }
        }
    }

    _getGTINFromEPC(epc) {
        var gs1GTINType = ':idpat:sgtin:';
        var gs1LotEPCType = ':id:lgtin:';
        var gs1SerialEPCType = ':id:sgtin:';
        var gdstGTINType = ':product:class:';
        var gdstLotEPCType = ':product:lot:class:';
        var gdstSerialEPCType = ':product:serial:obj:';

        // strip the end off the epc
        var pieces = epc.split('.');
        var gtin = '';
        for (var i = 0; i < (pieces.length - 1); i++) {
            if (i > 0) {
                gtin += '.';
            }
            gtin += pieces[i];
        }

        // replace the data type
        if (gtin.indexOf(gs1LotEPCType) >= 0) {
            gtin = gtin.replace(gs1LotEPCType, gs1GTINType);
        }
        else if (gtin.indexOf(gs1SerialEPCType) >= 0) {
            gtin = gtin.replace(gs1SerialEPCType, gs1GTINType);
        }
        else if (gtin.indexOf(gdstLotEPCType) >= 0) {
            gtin = gtin.replace(gdstLotEPCType, gdstGTINType);
        }
        else if (gtin.indexOf(gdstSerialEPCType) >= 0) {
            gtin = gtin.replace(gdstSerialEPCType, gdstGTINType);
        }

        // return the gtin
        return gtin
    }

    _buildProductFromQuantityList(quantity) {
        var self = this;
        var epc = quantity.epcClass
        var gtin = this._getGTINFromEPC(epc);
        var lotNumber = epc.split('.').pop();
        var weight = parseFloat(parseFloat(quantity.quantity) ?? 0);
        var uom = GDSTMeasurement.UOMs.find(u => u.UNCode === quantity.uom);

        // look up the product definition
        var productDef = self._lookUpProductDefinition(gtin);

        // build the product
        var product = {
            Product: productDef,
            LotNumber: lotNumber,
            NetWeight: {
                Value: weight,
                UoM: uom
            }
        };

        return product;
    }
}