class GDSTXmlImporter {

    constructor(element) {
        var self = this;

        this._window = $(element).kendoWindow({
            visible: false,
            width: "600px",
            title: 'EPCIS XML Import'
        }).data('kendoWindow');


        this._vm = new kendo.observable({
            Import: function (e) {
                var upload = $("#import").data('kendoUpload');
                if (upload.getFiles().length > 0) {
                    var files = upload.getFiles();
                    for (var i = 0; i < files.length; i++) {
                        var selectedFile = files[i];
                        const reader = new FileReader();
                        reader.addEventListener('load', (event) => {
                            var text = event.target.result;
                            self._importXML(text);
                            self._window.close();
                        });
                        reader.readAsText(selectedFile.rawFile);
                    }
                }
                else {
                    GDST.Util.Alert('Error', 'No file selected.');
                }
            }
        });
        kendo.bind(element, this._vm);
    }

    Open() {
        this._window.center().open();
    }

    _prefixResolver(prefix) {
        if (prefix === 'gdst') {
            return 'https://traceability-dialogue.org/epcis';
        }
        else if (prefix === 'cbvmda') {
            return 'urn:epcglobal:cbv:mda';
        }
    }

    _importXML(text) {
        var self = this;

        var xml = $($.parseXML(text));

        self._productDefs = self._readProductDefinitions(xml);
        self._locations = self._readLocations(xml);
        self._tradingParties = self._readTradingParties(xml);

        self._observeEvents = self._readObserveEvents(xml);
        self._transformEvents = self._readTransformEvents(xml);
        self._aggregateEvents = self._readAggregateEvents(xml);
        self._ctes = self._observeEvents.concat(self._transformEvents).concat(self._aggregateEvents);

        self._shapes = self._convertEventsToShapes(self._ctes);
        self._connections = self._convertEventsToConnections(self._ctes);

        var data = {
            MasterData: {
                ProductDefinitions: self._productDefs,
                Locations: self._locations,
                TradingParties: self._tradingParties
            },
            CTEs: self._ctes,
            Shapes: self._shapes,
            Connections: self._connections
        }

        // load the data into the web application
        GDSTMasterData.TradingParties = new kendo.data.ObservableArray(data.MasterData.TradingParties ?? []);
        GDSTMasterData.ProductDefinitions = new kendo.data.ObservableArray(data.MasterData.ProductDefinitions ?? []);
        GDSTMasterData.Locations = new kendo.data.ObservableArray(data.MasterData.Locations ?? []);
        GDST.WebApplication.MasterDataEditor._vm.set('TradingParties', GDSTMasterData.TradingParties);
        GDST.WebApplication.MasterDataEditor._vm.set('ProductDefinitions', GDSTMasterData.ProductDefinitions);
        GDST.WebApplication.MasterDataEditor._vm.set('Locations', GDSTMasterData.Locations);
        GDST.WebApplication.Diagram.KendoDiagram.clear();
        for (var i = 0; i < data.Shapes.length; i++) {
            GDST.WebApplication.Diagram.KendoDiagram.addShape(data.Shapes[i]);
        }
        GDST.WebApplication.Diagram.RebindShapes();

        // generate all the connections...
        var connectionsMap = new Map();
        var connections = [];
        var determineIfLinkExists = function(list, pi) {
            for(var i = 0; i < list.length; i++) {
                if(list[i].Product?.GTIN && list[i].LotNumber) {
                    if(list[i].Product?.GTIN === pi.Product?.GTIN && list[i].LotNumber === pi.LotNumber) {
                        return true;
                    }
                }
            }
            return false;
        }
        var findClosestEvent = function (shapes, currentShape, currentPI, filter) {

            // filter the shapes to only events that occured beefore the currentShape...
            var filteredShapes = shapes.filter(s => Date.parse(s.dataItem.EventTime) < Date.parse(currentShape.dataItem.EventTime))
            filteredShapes = filteredShapes.sort((a,b) =>  {
                var dParseA = Date.parse(a.dataItem.EventTime);
                var dParseB  = Date.parse(b.dataItem.EventTime);
                if(dParseA === dParseB) {
                    return 0;
                }
                else if(dParseA > dParseB) {
                    return 1;
                }
                else {
                    return -1;
                }
            });

            // find the event that comes right before this one where the filter returns true
            // cannot return the same event as the current event
            var date = currentShape.dataItem.EventTime;
            date = (date instanceof Date) ? date : new Date(Date.parse(date));
            var matchShape = null;
            var matchDate = null;
            for (var i = 0; i < filteredShapes.length; i++) {
                var s = filteredShapes[i];
                var d = s.dataItem.EventTime;
                d = (d instanceof Date) ? d : new Date(Date.parse(d));
                if (d.getTime() < date.getTime()) {
                    var filterResult = filter(s.dataItem, currentPI);
                    if(filterResult === 'stop') {
                        break;
                    }
                    if (filterResult === true) {
                        if (matchDate && matchShape) {
                            if (matchDate.getTime() < d.getTime()) {
                                matchShape = s;
                                matchDate = d;
                            }
                        }
                        else {
                            matchShape = s;
                            matchDate = d;
                        }
                    }
                }
            }

            // return null if nothing found that meets the filter with an event time before the current event
            return matchShape;
        }

        // sort the events by their event time...
        var connections = [];
        for (var i = 0; i < GDST.WebApplication.Diagram.KendoDiagram.shapes.length; i++) {
            var shape = GDST.WebApplication.Diagram.KendoDiagram.shapes[i];
            var cte = shape.dataItem;

            // foreach input...
            // find the event with the closest event time that is before the current event where the 
            // input is either in the outputs, references, or the inputs of an disaggregate event
            // if we find this event, then create a connection between that shape and this shape (target).
            var inputRefFilter = function (c, pi) {
                if (c.EventType.ID === 'DeaggregateEvent' && determineIfLinkExists(c.Inputs, pi)) {
                    return true;
                }
                else if (determineIfLinkExists(c.References, pi)) {
                    return true;
                }
                else if (determineIfLinkExists(c.Outputs, pi)) {
                    return true;
                }
                return false;
            };

            // if we have a Parent ID, and we are not an Aggregate event, then look for Ship, Receive, or Disaggregate
            // with the closest event time that is before the current event where the ParentID matches this event's 
            // parent ID.
            var parentFilter = function (c) {
                if (c.ParentID === cte.ParentID) {
                    return true;
                }
                else if(c.Type === 'AggregateEvent') {
                    return 'stop';
                }
                return false;
            }
            
            if (cte.Type === 'AggregateEvent' || GDST.Util.IsStringNullOrEmpty(cte.ParentID)) {
                for (var j = 0; j < cte.References.length; j++) {
                    var matchingShape = findClosestEvent(GDST.WebApplication.Diagram.KendoDiagram.shapes, shape, cte.References[j], inputRefFilter);
                    if (matchingShape) {
                        connections.push({ from: matchingShape, to: shape});
                    }
                }
                for (var j = 0; j < cte.Inputs.length; j++) {
                    var matchingShape = findClosestEvent(GDST.WebApplication.Diagram.KendoDiagram.shapes, shape, cte.Inputs[j], inputRefFilter);
                    if (matchingShape) {
                        connections.push({ from: matchingShape, to: shape});
                    }
                }
            }
            else {
                var matchingShape = findClosestEvent(GDST.WebApplication.Diagram.KendoDiagram.shapes, shape, cte, parentFilter);
                if (matchingShape) {
                    connections.push({ from: matchingShape, to: shape});
                }
            }
        }

        // remove duplicate connections
        var uniqueConnections = [];
        for (var i = 0; i < connections.length; i++) {
            var conn = connections[i];
            if(uniqueConnections.find(c => c.to.id === conn.to.id && c.from.id === conn.from.id) === undefined)
            {
                uniqueConnections.push(conn);
            }
        }

        // add the connections
        for (var i = 0; i < uniqueConnections.length; i++) {
            // var connection = new kendo.dataviz.diagram.Connection(uniqueConnections[i].from, uniqueConnections[i].to);
            // connection.from.shape = uniqueConnections[i].from;
            // connection.to.shape = uniqueConnections[i].to;
            // connection.editable = true;
            // GDST.WebApplication.Diagram.KendoDiagram.addConnection(connection);
            GDST.WebApplication.Diagram.KendoDiagram.connect(uniqueConnections[i].from, uniqueConnections[i].to);
        }

        // refresh some stuff
        GDST.WebApplication.Diagram.KendoDiagram.layout({
            type: "layered",
            subtype: "right"
        });
        GDST.WebApplication.Diagram.KendoDiagram.bringIntoView(GDST.WebApplication.Diagram.KendoDiagram.shapes);
        GDSTMasterData.GenerateNewTradingPartyColors();
        GDSTFeedback.CanPerformTraceback(GDST.WebApplication.Diagram.KendoDiagram.shapes, GDST.WebApplication.Diagram.KendoDiagram.connections);

        console.log(data);
        return data;
    }

    /** Reads the xml and extracts any product definitions from the XML. */
    _readProductDefinitions(xml) {
        var productDefs = [];
        xml.xpath("//Vocabulary[@type='urn:epcglobal:epcis:vtype:EPCClass']/VocabularyElementList/VocabularyElement").each(function () {
            var productDef = {};
            productDef.GTIN = $(this).attr('id');
            productDef.UUID = GDST.Util.GenerateUUID();
            productDef.Name = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#descriptionShort"]').text();
            productDef.Species = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#speciesForFisheryStatisticsPurposesName"]').text();
            productDef.ProductForm = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#additionalTradeItemIdentification"]').text();
            productDefs.push(productDef);
        });
        return productDefs;
    }

    /** Reads the xml and extracts any product definitions from the XML. */
    _readLocations(xml) {
        var locations = [];
        xml.xpath("//Vocabulary[@type='urn:epcglobal:epcis:vtype:Location']/VocabularyElementList/VocabularyElement").each(function () {

            // create the location from the xml
            var location = {};
            location.GLN = $(this).attr('id');
            location.UUID = GDST.Util.GenerateUUID();
            location.Name = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#name"]').text();
            location.Address1 = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#streetAddressOne"]').text();
            location.Address2 = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#streetAddressTwo"]').text();
            location.City = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#city"]').text();
            location.State = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#state"]').text();
            location.PostalCode = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#zipcode"]').text();
            location.Country = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#countryCode"]').text();
            location.VesselID = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#vesselID"]').text();
            location.IMONumber = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#imoNumber"]').text();
            location.VesselFlag = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#vesselFlagState"]').text();

            locations.push(location);
        });
        return locations;
    }

    _readTradingParties(xml) {
        var tps = [];
        xml.xpath("//Vocabulary[@type='urn:epcglobal:epcis:vtype:TradingParty']/VocabularyElementList/VocabularyElement").each(function () {

            // create the location from the xml
            var tp = {};
            tp.PGLN = $(this).attr('id');
            tp.Name = $(this).xpath('attribute[@id="urn:epcglobal:cbv:mda#name"]').text();
            tp.UUID = GDST.Util.GenerateUUID();

            tps.push(tp);
        });
        console.log(tps);
        return tps;
    }

    /** Read the observe events from the XML. */
    _readObserveEvents(xml) {
        var self = this;

        var events = [];
        xml.xpath("//ObjectEvent").each(function () {
            var xEvent = this;

            var cte = {};

            // look up the event type by the biz step
            var bizStep = $(xEvent).xpath('bizStep').text();
            var eventType = GDST.EventTypes.FindByBizStep(bizStep);
            cte.Type = eventType.ID;

            // get the guid
            cte.Guid = $(xEvent).xpath('baseExtension/eventID').text();

            // read the epc list
            cte.References = [];
            cte.Inputs = [];
            cte.Outputs = [];
            $(xEvent).xpath('epcList/epc').each(function () {
                var epc = $(this).text();
                if (epc.startsWith('urn:sscc:')) {
                    cte.ParentID = epc;
                }
                else {

                    // add the product to the cte
                    var product = self._buildProductFromEPCList(epc);
                    cte.References.push(product);
                }
            });

            // read the quantity list
            $(xEvent).xpath('extension/quantityList/quantityElement').each(function () {
                var xQuantity = $(this);
                var product = self._buildProductFromQuantityList(xQuantity);
                cte.References.push(product);
            })

            // read the event time
            cte.EventTime = $(xEvent).xpath('eventTime').text()

            // set the event type
            cte.EventType = eventType;

            // look up the location by the gln
            var gln = $(xEvent).xpath('bizLocation/id').text();
            cte.Location = self._lookUpLocation(gln);

            // read the certification list
            $(xEvent).xpath('gdst:certificationList/cbvmda:certification', self._prefixResolver).each(function () {
                var xCertificate = $(this);
                self._readCertificate(xCertificate, cte);
            });

            // read the certification list
            $(xEvent).xpath('extension/ilmd/cbvmda:certificationList/cbvmda:certification', self._prefixResolver).each(function () {
                var xCertificate = $(this);
                self._readCertificate(xCertificate, cte);
            });

            // read the information provider
            var infoProviderPGLN = $(xEvent).xpath('cbvmda:informationProvider', self._prefixResolver).text();
            cte.InformationProvider = self._lookUpTradingParty(infoProviderPGLN);

            // read the seller
            var sellerPGLN = $(xEvent).xpath('extension/sourceList/source[@type="urn:epcglobal:cbv:sdt:owning_party"]', self._prefixResolver).text();
            cte.Seller = self._lookUpTradingParty(sellerPGLN);

            // read the buyer
            var buyerPGLN = $(xEvent).xpath('extension/destinationList/destination[@type="urn:epcglobal:cbv:sdt:owning_party"]', self._prefixResolver).text();
            cte.Buyer = self._lookUpTradingParty(buyerPGLN);

            // read the product owner
            var productOwnerPGLN = $(xEvent).xpath('gdst:productOwner', self._prefixResolver).text();
            cte.ProductOwner = self._lookUpTradingParty(productOwnerPGLN);

            // read any additional KDEs using the event type
            self._readKDEs(xEvent, cte);

            events.push(cte);
        });

        return events;
    }

    /** Read the transform events from the XML. */
    _readTransformEvents(xml) {
        var self = this;
        var events = [];
        xml.xpath("//TransformationEvent").each(function () {
            var xEvent = this;

            var cte = {};

            // look up the event type by the biz step
            var bizStep = $(xEvent).xpath('bizStep').text();
            var eventType = GDST.EventTypes.FindByBizStep(bizStep);
            cte.Type = eventType.ID;

            // get the guid
            cte.Guid = $(xEvent).xpath('baseExtension/eventID').text();

            // read the epc list
            cte.References = [];
            cte.Inputs = [];
            cte.Outputs = [];

            // read the quantity list
            $(xEvent).xpath('outputQuantityList/quantityElement').each(function () {
                var xQuantity = $(this);
                var product = self._buildProductFromQuantityList(xQuantity);
                cte.Outputs.push(product);
            });
            $(xEvent).xpath('inputQuantityList/quantityElement').each(function () {
                var xQuantity = $(this);
                var product = self._buildProductFromQuantityList(xQuantity);
                cte.Inputs.push(product);
            });

            // read the event time
            cte.EventTime = $(xEvent).xpath('eventTime').text();

            // set the event type
            cte.EventType = eventType;

            // look up the location by the gln
            var gln = $(xEvent).xpath('bizLocation/id').text();
            cte.Location = self._lookUpLocation(gln);

            // read the certification list
            cte.Certificates = {};
            $(xEvent).xpath('ilmd/cbvmda:certificationList/cbvmda:certification', self._prefixResolver).each(function () {
                var xCertificate = $(this);
                self._readCertificate(xCertificate, cte);
            });

            // read the information provider
            var infoProviderPGLN = $(xEvent).xpath('cbvmda:informationProvider', self._prefixResolver).text();
            cte.InformationProvider = self._lookUpTradingParty(infoProviderPGLN);

            // read the seller
            var sellerPGLN = $(xEvent).xpath('sourceList/source[@type="urn:epcglobal:cbv:sdt:owning_party"]', self._prefixResolver).text();
            cte.Seller = self._lookUpTradingParty(sellerPGLN);

            // read the buyer
            var buyerPGLN = $(xEvent).xpath('destinationList/destination[@type="urn:epcglobal:cbv:sdt:owning_party"]', self._prefixResolver).text();
            cte.Buyer = self._lookUpTradingParty(buyerPGLN);

            // read the product owner
            var productOwnerPGLN = $(xEvent).xpath('gdst:productOwner', self._prefixResolver).text();
            cte.ProductOwner = self._lookUpTradingParty(productOwnerPGLN);

            // read any additional KDEs using the event type
            self._readKDEs(xEvent, cte);

            events.push(cte);
        });

        return events;
    }

    /** Read the aggregate events from the XML. */
    _readAggregateEvents(xml) {
        var self = this;
        var events = [];
        xml.xpath("//AggregationEvent").each(function () {
            var xEvent = this;

            var cte = {};

            // look up the event type by the biz step
            var bizStep = $(xEvent).xpath('bizStep').text();
            var eventType = GDST.EventTypes.FindByBizStep(bizStep);
            cte.Type = eventType.ID;

            // get the guid
            cte.Guid = $(xEvent).xpath('baseExtension/eventID').text();

            // read the epc list
            cte.References = [];
            cte.Inputs = [];
            cte.Outputs = [];

            // read the quantity list
            $(xEvent).xpath('extension/childQuantityList/quantityElement').each(function () {
                var xQuantity = $(this);
                var product = self._buildProductFromQuantityList(xQuantity);
                cte.Inputs.push(product);
            });

            // read the parent ID
            cte.ParentID = $(xEvent).xpath('parentID').text();

            // read the event time
            cte.EventTime = $(xEvent).xpath('eventTime').text()

            // set the event type
            cte.EventType = eventType;

            // look up the location by the gln
            var gln = $(xEvent).xpath('bizLocation/id').text();
            cte.Location = self._lookUpLocation(gln);

            // read the certification list
            $(xEvent).xpath('extension/cbvmda:certificationList/cbvmda:certification', self._prefixResolver).each(function () {
                var xCertificate = $(this);
                self._readCertificate(xCertificate, cte);
            });

            // read the information provider
            var infoProviderPGLN = $(xEvent).xpath('cbvmda:informationProvider', self._prefixResolver).text();
            cte.InformationProvider = self._lookUpTradingParty(infoProviderPGLN);

            // read the seller
            var sellerPGLN = $(xEvent).xpath('sourceList/source[@type="urn:epcglobal:cbv:sdt:owning_party"]', self._prefixResolver).text();
            cte.Seller = self._lookUpTradingParty(sellerPGLN);

            // read the buyer
            var buyerPGLN = $(xEvent).xpath('destinationList/destination[@type="urn:epcglobal:cbv:sdt:owning_party"]', self._prefixResolver).text();
            cte.Buyer = self._lookUpTradingParty(buyerPGLN);

            // read the product owner
            var productOwnerPGLN = $(xEvent).xpath('gdst:productOwner', self._prefixResolver).text();
            cte.ProductOwner = self._lookUpTradingParty(productOwnerPGLN);

            // read any additional KDEs using the event type
            self._readKDEs(xEvent, cte);

            events.push(cte);
        });

        return events;
    }

    _lookUpLocation(gln) {
        var location = this._locations.find(l => l.GLN === gln);
        return location;
    }

    _lookUpTradingParty(pgln) {
        var tp = this._tradingParties.find(t => t.PGLN === pgln);
        return tp;
    }

    _lookUpProductDefinition(gtin) {
        var productDef = this._productDefs.find(p => p.GTIN === gtin);
        return productDef;
    }

    _getGTINFromEPC(epc) {
        var gs1GTINType = ':idpat:sgtin:';
        var gs1LotEPCType = ':id:lgtin:';
        var gs1SerialEPCType = ':id:sgtin:';
        var gdstGTINType = ':product:class:';
        var gdstLotEPCType = ':product:lot:class:';
        var gdstSerialEPCType = ':product:serial:obj:';

        // strip the end off the epc
        var pieces = epc.split('.');
        var gtin = '';
        for (var i = 0; i < (pieces.length - 1); i++) {
            if (i > 0) {
                gtin += '.';
            }
            gtin += pieces[i];
        }

        // replace the data type
        if (gtin.indexOf(gs1LotEPCType) >= 0) {
            gtin = gtin.replace(gs1LotEPCType, gs1GTINType);
        }
        else if (gtin.indexOf(gs1SerialEPCType) >= 0) {
            gtin = gtin.replace(gs1SerialEPCType, gs1GTINType);
        }
        else if (gtin.indexOf(gdstLotEPCType) >= 0) {
            gtin = gtin.replace(gdstLotEPCType, gdstGTINType);
        }
        else if (gtin.indexOf(gdstSerialEPCType) >= 0) {
            gtin = gtin.replace(gdstSerialEPCType, gdstGTINType);
        }

        // return the gtin
        return gtin
    }

    _buildProductFromEPCList(epc) {
        var self = this;

        // convert the EPC into a GTIN
        var gtin = self._getGTINFromEPC(epc);
        var serialNumber = epc.split('.').pop();

        // look up the product definition
        var productDef = self._lookUpProductDefinition(gtin);

        // build the product
        var product = {
            ProductDefinition: productDef,
            SerialNumber: serialNumber
        };

        return product;
    }

    _buildProductFromQuantityList(xQuantity) {
        var self = this;
        var epc = xQuantity.xpath('epcClass').text();
        var gtin = this._getGTINFromEPC(epc);
        var lotNumber = epc.split('.').pop();
        var weight = parseFloat(xQuantity.xpath('quantity').text() ?? 0);
        var uom = GDSTMeasurement.UOMs.find(u => u.UNCode === xQuantity.xpath('uom').text());


        // look up the product definition
        var productDef = self._lookUpProductDefinition(gtin);

        // build the product
        var product = {
            Product: productDef,
            LotNumber: lotNumber,
            NetWeight: {
                Value: weight,
                UoM: uom
            }
        };

        return product;
    }

    _readCertificate(xCertificate, cte) {
        var certificate = {};
        var self = this;
        var certType = xCertificate.xpath('gdst:certificateType', self._prefixResolver).text() ?? "";
        var certName = "";
        for (var prop in cte.EventType.Certificates) {
            var cert = cte.EventType.Certificates[prop];
            if (cert.Type == certType) {
                certName = prop;
                break;
            }
        }
        certificate.Standard = xCertificate.xpath('cbvmda:certificationStandard', self._prefixResolver).text() ?? "";
        certificate.Agency = xCertificate.xpath('cbvmda:certificationAgency', self._prefixResolver).text() ?? "";
        certificate.Identification = xCertificate.xpath('cbvmda:certificationIdentification', self._prefixResolver).text() ?? "";
        certificate.Value = xCertificate.xpath('cbvmda:certificationValue', self._prefixResolver).text() ?? "";
        if (!cte.Certificates) cte.Certificates = {};
        cte.Certificates[certName] = certificate;
    }

    _readKDEs(xEvent, cte) {
        var self = this;
        for (var kdeProp in cte.EventType.KDEs) {
            var kde = cte.EventType.KDEs[kdeProp];
            var xpath = kde.XPath;
            var value = $(xEvent).xpath(xpath, self._prefixResolver).text();
            if (!GDST.Util.IsStringNullOrEmpty(value)) {
                //if(kde.Type === 'date') {
                //    value = new Date(Date.parse(value));
                //}
                if(kde.Type === 'coordinates') {
                    var pieces = value.substring(4)?.split(',');
                    value = {
                        Latitude: parseFloat(pieces[0]),
                        Longitude: parseFloat(pieces[0])
                    };
                }
                cte[kdeProp] = value;
            }
        }
    }

    _convertEventsToShapes(events) {
        var shapes = [];
        for (var i = 0; i < events.length; i++) {
            shapes.push({
                dataItem: events[i],
                x: 0,
                y: 0
            });
        }
        return shapes;
    }

    _convertEventsToConnections(events) {
        return [];
    }
}