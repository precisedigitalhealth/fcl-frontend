class GDSTFeedback {

    /** Returns TRUE if the CTE is completely filled in, otherwise returns FALSE. */
    static IsCTEComplete(cte) {

        // verify they selected a location
        if(GDST.Util.IsStringNullOrEmpty(cte.Location) || cte.Location === '$blank_option$')
        {
            return false;
        }

        // verify they selected a product owner
        if(GDST.Util.IsStringNullOrEmpty(cte.ProductOwner) || cte.ProductOwner === '$blank_option$')
        {
            return false;
        }

        // verify they selected a information provider
        if(GDST.Util.IsStringNullOrEmpty(cte.InformationProvider) || cte.InformationProvider === '$blank_option$')
        {
            return false;
        }

        // go through the kde..
        for(var kdeProperty in cte.EventType.KDEs)
        {
            var kde = cte.EventType.KDEs[kdeProperty];
            if(kde.Type)
            {
                var value = cte[kdeProperty];
                switch(kde.Type) {
                    case "string":
                    case "date": {
                        if(GDST.Util.IsStringNullOrEmpty(value) === true)
                        {
                            return false;
                        }
                        break;
                    }
                    case "dropdownlist": {
                        if(GDST.Util.IsStringNullOrEmpty(value) === true || value === '$blank_option$')
                        {
                            return false;
                        }
                        break;
                    }
                    case "coordinates": {
                        if(value.Latitude === null || value.Latitude === undefined || GDST.Util.IsStringNullOrEmpty(value.Latitude.toString()) ||
                           value.Longitude === null || value.Longitude === undefined || GDST.Util.IsStringNullOrEmpty(value.Longitude.toString()))
                           {
                               return false;
                           }
                    }
                }
            }
        }

        // go through each certificate
        for(var certProperty in cte.EventType.Certificates)
        {
            var cert = cte.EventType.Certificates[certProperty];
            if(cert.Type)
            {
                if(cte.Certificates)
                {
                    var value = cte.Certificates[certProperty];
                    if(GDST.Util.IsStringNullOrEmpty(value.Standard) || GDST.Util.IsStringNullOrEmpty(value.Value))
                    {
                        return false;
                    }
                }
            }
        }

        // create a method that we will use for checking if a product is complete
        var isCompleteProduct = function(product) { 
            if (GDST.Util.IsStringNullOrEmpty(product.Product))
            {
                return false;
            }
            if (GDST.Util.IsStringNullOrEmpty(product.LotNumber) && GDST.Util.IsStringNullOrEmpty(product.SerialNumber))
            {
                return false;
            }
            return true;
        };

        // if this is a references type cte
        if(cte.EventType.Type.toLowerCase().trim() === 'references')
        {
            if(cte.EventType.AllowSSCCs === true && !GDST.Util.IsStringNullOrEmpty(cte.ParentID)) 
            {
                // don't do anything because this is means it is complete in regards to product references
            }
            else if (cte.References && cte.References.length < 1)
            {
                return false;
            }    
            else {
                for(var i = 0; i < cte.References.length; i++)
                {
                    if(isCompleteProduct(cte.References[i]) === false)
                    {
                        return false;
                    }
                }
            }
        }
        else {
            if (cte.Inputs && cte.Inputs.length < 1)
            {
                return false;
            }
            for(var i = 0; i < cte.Inputs.length; i++)
            {
                if(isCompleteProduct(cte.Inputs[i]) === false)
                {
                    return false;
                }
            }
            if (cte.EventType.Type === 'InputsAndOutputs' && cte.Outputs && cte.Outputs.length < 1)
            {
                return false;
            }
            for(var i = 0; i < cte.Outputs.length; i++)
            {
                if(isCompleteProduct(cte.Outputs[i]) === false)
                {
                    return false;
                }
            }
        }

        // if we got all the way here, then we will return TRUe indicating it is compelte
        return true;
    }

    /** Returns TRUE is a traceback could be performed successfully, otherwise returns FALSE. */
    static CanPerformTraceback(shapes, connections) {

        var errors = [];

        // find all nodes where there are no connections where they are the "to" in the connection
        var endNodes = shapes.filter(s => !connections.some(c => c.from?.shape?.id === s.id));
        
        // if there are more than one end nodes, then we need to return an error
        // if(endNodes.length > 1)
        // {
        //     errors.push("There is more than one end node.");
        // }

        // this is a reusable method for determining if 2 products are the same product instance
        var areProductsEqualFunc = function(prod1, prod2) {
            if(prod1.Product && prod2.Product && prod1.Product?.GTIN === prod2.Product?.GTIN)
            {
                if((!GDST.Util.IsStringNullOrEmpty(prod1.LotNumber) && !GDST.Util.IsStringNullOrEmpty(prod2.LotNumber) && prod1.LotNumber === prod2.LotNumber) 
                ) // || (!GDST.Util.IsStringNullOrEmpty(prod1.SerialNumber) && !GDST.Util.IsStringNullOrEmpty(prod2.SerialNumber) && prod1.SerialNumber === prod2.SerialNumber))
                {
                    return true;
                }
            }
            return false;
        }

        // loop through each connection
        for(var i = 0; i < connections.length; i++)
        {
            var currentConnection = connections[i];
            
            // determine if the fromShape and the toShape share a product instance connection
            var isConnectionValid = false;
            var isConnectionCircular = false;

            currentConnection.to.shape = currentConnection.to.shape ?? currentConnection.to;
            currentConnection.from.shape = currentConnection.from.shape ?? currentConnection.from;
            if(currentConnection.to.shape && currentConnection.from.shape)
            {
                var toShape = currentConnection.to.shape;
                var fromShape = currentConnection.from.shape;
                if(fromShape.id != toShape.id && toShape.dataItem && fromShape.dataItem) {
                    var fromList = [];
                    var toList = [];

                    if (fromShape.dataItem.EventType.Type.toLowerCase().trim() === 'references')
                    {
                        fromList = fromShape.dataItem.References;
                    }
                    else
                    {
                        fromList = fromShape.dataItem.Outputs;
                    }

                    if (toShape.dataItem.EventType.Type.toLowerCase().trim() === 'references')
                    {
                        toList = toShape.dataItem.References;
                    }
                    else
                    {
                        toList = toShape.dataItem.Inputs;
                    }

                    if (fromShape.dataItem.EventType.ID === 'DeaggregateEvent') {
                        fromList = fromShape.dataItem.Inputs;
                    }
    
                    // if there is a valid traceback from the fromList to the toList, then we set the connection as valid...
                    if (fromList.some(p => toList.some(p2 => areProductsEqualFunc(p, p2))))
                    {
                        isConnectionValid = true;
                    }
                    else if (!GDST.Util.IsStringNullOrEmpty(fromShape.dataItem.ParentID) 
                          && !GDST.Util.IsStringNullOrEmpty(toShape.dataItem.ParentID) 
                          && fromShape.dataItem.ParentID === toShape.dataItem.ParentID)
                    {
                        isConnectionValid = true;
                    }

                    
                }
                else {
                    isConnectionCircular = true;
                }
            }

            // redraw the connection
            currentConnection.redraw({
                stroke: {
                    color: (isConnectionValid) ? 'black' : 'red',
                    width: 2
                },
                endCap: {
                    stroke: {
                        width: 4,
                    },
                    type: "ArrowEnd",
                    fill: {
                        color: (isConnectionValid) ? 'black' : 'red'
                    }
                }
            });

            // add error if connection is not valid
            if (isConnectionValid === false){
                errors.push(`The connection between a ${fromShape?.dataItem?.EventType.Title} CTE and a ${toShape?.dataItem?.EventType.Title} CTE failed a traceback.`);
            }
        }

        // return any errors if there are some
        return errors;
    }

    constructor(settings) {
        var self = this;
        self.Element = settings.element;
        
        // initialize the kendo window
        self._window = self.Element.kendoWindow({
            title: 'Feedback',
            height: '75%',
            width: '75%',
            visible: false,
            open: function (e) {
                e.sender.setOptions({
                    height: $(window).height() * 0.75,
                    width: $(window).width() * 0.75
                });
                e.sender.center();
            },
        }).data('kendoWindow');
    }

    Open(diagram) {
        this._window.center().open();

        // map the shapes into a list a of ctes
        var ctes = diagram.shapes.map(s => s.dataItem);

        // keep a list of feedback items
        var feedbackItems = [];

        // check if there are incomplete CTEs
        var hasIncompleteCTEs = ctes.some(c => GDSTFeedback.IsCTEComplete(c) === false);
        if(hasIncompleteCTEs === true)
        {
            feedbackItems.push(new GDSTFeedbackItem("There are 1 or more CTEs that have incomplete information filled in.", 'error'));
        }

        // check if we can perform a traceback
        var tracebackErrors = GDSTFeedback.CanPerformTraceback(diagram.shapes, diagram.connections);
        for(var i = 0; i < tracebackErrors.length; i++)
        {
            feedbackItems.push(new GDSTFeedbackItem(tracebackErrors[i], 'error'));
        }

        // build the list
        var html = '';
        for(var i = 0; i < feedbackItems.length; i++)
        {
            html += feedbackItems[i].ToHTML();
        }
        this.Element.find('.feedback-list').html(html);
    }

    get Window() {
        return this._window;
    }
}

class GDSTFeedbackItem {
    constructor(text, type) {
        this.Text = text;
        this.Type = type;
    }

    ToHTML() {
        var icon = "fa-exclamation-triangle";
        var iconColor = "red";
        switch(this.Type) {
            
        }
        var html = `
            <div style="border-bottom: 1px solid lightgrey; display: flex; flex-direction: row;">
                <div style="vertical-align: middle; padding-top: 0.1em;">
                    <h1><i class="fa ${icon}" style="color: ${iconColor}; vertical-align: middle; padding: 0.2em 1.0em 0.0em 1.0em;"></i></h1>
                </div>
                <div style="flex: 1;">
                    <h5 style="font-weight: normal; font-size: 1.2em;">${this.Text}</h5>
                </div>
            </div>
        `;
        return html;
    }
}