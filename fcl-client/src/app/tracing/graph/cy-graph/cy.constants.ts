export const PRESET_LAYOUT_NAME = 'preset';

export const NODE_GROUP = 'nodes';
export const EDGE_GROUP = 'edges';

export const CY_MIN_ZOOM = 1e-50;
export const CY_MAX_ZOOM = 1e50;

export const CY_EVENT_PAN = 'pan';
export const CY_EVENT_TAP_START = 'tapstart';
export const CY_EVENT_TAP_END = 'tapend';
export const CY_EVENT_TAP_SELECT = 'tapselect';
export const CY_EVENT_TAP_UNSELECT = 'tapunselect';
export const CY_EVENT_BOX_SELECT = 'boxselect';
export const CY_EVENT_DRAG_FREE_ON = 'dragfreeon';
export const CY_EVENT_ZOOM = 'zoom';
export const CY_EVENT_CXT_TAP = 'cxttap';
