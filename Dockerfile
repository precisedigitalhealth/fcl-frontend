FROM nginx:latest

COPY ./GDST /usr/share/nginx/html/IT-Conversion-Mapping-Tool

COPY ./fcl-client/public /usr/share/nginx/html/fcl

EXPOSE 80

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]